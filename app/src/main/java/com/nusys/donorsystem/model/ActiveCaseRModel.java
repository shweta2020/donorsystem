package com.nusys.donorsystem.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created Date:10-07-2020
 * Updated Date:
 * Updated Date:
 **/
public class ActiveCaseRModel {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("case_title")
        @Expose
        private String caseTitle;
        @SerializedName("cat_name")
        @Expose
        private String catName;
        @SerializedName("required_money")
        @Expose
        private String requiredMoney;
        @SerializedName("city_name")
        @Expose
        private String cityName;
        @SerializedName("KYC_status")
        @Expose
        private String kYCStatus;
        @SerializedName("status")
        @Expose
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCaseTitle() {
            return caseTitle;
        }

        public void setCaseTitle(String caseTitle) {
            this.caseTitle = caseTitle;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public String getRequiredMoney() {
            return requiredMoney;
        }

        public void setRequiredMoney(String requiredMoney) {
            this.requiredMoney = requiredMoney;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getKYCStatus() {
            return kYCStatus;
        }

        public void setKYCStatus(String kYCStatus) {
            this.kYCStatus = kYCStatus;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }
}
