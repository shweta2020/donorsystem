package com.nusys.donorsystem.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created Date:01-07-2020
 * Updated Date:
 */
public class UserTypeModel {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("user_typeid")
        @Expose
        private String userTypeid;
        @SerializedName("user_typename")
        @Expose
        private String userTypename;

        public String getUserTypeid() {
            return userTypeid;
        }

        public void setUserTypeid(String userTypeid) {
            this.userTypeid = userTypeid;
        }

        public String getUserTypename() {
            return userTypename;
        }

        public void setUserTypename(String userTypename) {
            this.userTypename = userTypename;
        }

    }
}
