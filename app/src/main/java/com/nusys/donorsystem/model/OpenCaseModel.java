package com.nusys.donorsystem.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created Date:06-07-2020
 * Updated Date:
 * Updated Date:
 **/
public class OpenCaseModel {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("case_id")
        @Expose
        private String caseId;
        @SerializedName("case_title")
        @Expose
        private String caseTitle;
        @SerializedName("case_disc")
        @Expose
        private String caseDisc;
        @SerializedName("required_money")
        @Expose
        private String requiredMoney;
        @SerializedName("case_category")
        @Expose
        private String caseCategory;
        @SerializedName("date_from")
        @Expose
        private String dateFrom;
        @SerializedName("date_to")
        @Expose
        private String dateTo;
        @SerializedName("img")
        @Expose
        private String img;

        public String getCaseId() {
            return caseId;
        }

        public void setCaseId(String caseId) {
            this.caseId = caseId;
        }

        public String getCaseTitle() {
            return caseTitle;
        }

        public void setCaseTitle(String caseTitle) {
            this.caseTitle = caseTitle;
        }

        public String getCaseDisc() {
            return caseDisc;
        }

        public void setCaseDisc(String caseDisc) {
            this.caseDisc = caseDisc;
        }

        public String getRequiredMoney() {
            return requiredMoney;
        }

        public void setRequiredMoney(String requiredMoney) {
            this.requiredMoney = requiredMoney;
        }

        public String getCaseCategory() {
            return caseCategory;
        }

        public void setCaseCategory(String caseCategory) {
            this.caseCategory = caseCategory;
        }

        public String getDateFrom() {
            return dateFrom;
        }

        public void setDateFrom(String dateFrom) {
            this.dateFrom = dateFrom;
        }

        public String getDateTo() {
            return dateTo;
        }

        public void setDateTo(String dateTo) {
            this.dateTo = dateTo;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

    }
}
