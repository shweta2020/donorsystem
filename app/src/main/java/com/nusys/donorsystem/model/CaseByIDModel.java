package com.nusys.donorsystem.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**Created By: Shweta Agarwal
 * Created Date:13-07-2020
 * Updated Date:15-07-2-2020(add cat name and city name)
 * Updated Date:
 **/
public class CaseByIDModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("case_id")
        @Expose
        private String caseId;
        @SerializedName("case_title")
        @Expose
        private String caseTitle;
        @SerializedName("case_disc")
        @Expose
        private String caseDisc;
        @SerializedName("required_money")
        @Expose
        private String requiredMoney;
        @SerializedName("city_Id")
        @Expose
        private String cityId;
        @SerializedName("city_name")
        @Expose
        private String cityName;
        @SerializedName("case_category_id")
        @Expose
        private String caseCategoryId;
        @SerializedName("cat_name")
        @Expose
        private String catName;
        @SerializedName("date_from")
        @Expose
        private String dateFrom;
        @SerializedName("date_to")
        @Expose
        private String dateTo;
        @SerializedName("image")
        @Expose
        private String image;

        public String getCaseId() {
            return caseId;
        }

        public void setCaseId(String caseId) {
            this.caseId = caseId;
        }

        public String getCaseTitle() {
            return caseTitle;
        }

        public void setCaseTitle(String caseTitle) {
            this.caseTitle = caseTitle;
        }

        public String getCaseDisc() {
            return caseDisc;
        }

        public void setCaseDisc(String caseDisc) {
            this.caseDisc = caseDisc;
        }

        public String getRequiredMoney() {
            return requiredMoney;
        }

        public void setRequiredMoney(String requiredMoney) {
            this.requiredMoney = requiredMoney;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getCityName() {
            return cityName;
        }

        public void setCityName(String cityName) {
            this.cityName = cityName;
        }

        public String getCaseCategoryId() {
            return caseCategoryId;
        }

        public void setCaseCategoryId(String caseCategoryId) {
            this.caseCategoryId = caseCategoryId;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public String getDateFrom() {
            return dateFrom;
        }

        public void setDateFrom(String dateFrom) {
            this.dateFrom = dateFrom;
        }

        public String getDateTo() {
            return dateTo;
        }

        public void setDateTo(String dateTo) {
            this.dateTo = dateTo;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }
}
