package com.nusys.donorsystem.commonModule;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.nusys.donorsystem.R;

import java.util.Calendar;

/**
 * Created By: Shweta Agarwal
 * Created DAte:30-06-2020
 **/
public class Extension {

    public static void showtoast(Context context, String message) {
        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
    }

    public static void printLog(String msg) {
        Log.e("error", msg);
    }

    public static void printResponse(String msg) {
        Log.e("response", msg);
    }

    //for calender
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void openCalender(Context context, int year, int month, int day, final TextView textView) {
        final Calendar c;
        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog mDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                textView.setText(new StringBuilder().append(selectedyear).append("-").append(selectedmonth + 1).append("-").append(selectedday));
            }
        }, year, month, day);
        mDatePicker.setTitle("Please select date");
//                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.show();


    }

    //for capitalize each word
    public static String getCapsSentences(String tagName) {
        String[] splits = tagName.toLowerCase().split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < splits.length; i++) {
            String eachWord = splits[i];
            if (i > 0 && eachWord.length() > 0) {
                sb.append(" ");
            }
            String cap = eachWord.substring(0, 1).toUpperCase()
                    + eachWord.substring(1);
            sb.append(cap);
        }
        //Toast.makeText(context, ""+sb.toString(), Toast.LENGTH_SHORT).show();
        //capText=sb.toString();
        return sb.toString();
    }

    public static void showErrorDialog(Context context, final Dialog dialog) {

//        dialog=new Dialog(context);
        dialog.setContentView(R.layout.error_dialog);
        TextView tvOk = dialog.findViewById(R.id.text_ok);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

    public static void showResponseErrorDialog(Context context, final Dialog dialog) {

//        dialog=new Dialog(context);
        dialog.setContentView(R.layout.error_dialog);
        TextView tvOk = dialog.findViewById(R.id.text_ok);
        TextView text = dialog.findViewById(R.id.text);
        text.setText("Something is wrong please try again later");
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }


}

