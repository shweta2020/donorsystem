package com.nusys.donorsystem.commonModule;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created By: Shweta Agarwal
 * Created DAte:01-07-2020
 **/
public class SharedPreference_main {

    Context mContext;
    private static SharedPreference_main sharedPreference_main;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;

    public SharedPreference_main(Context context) {
        mContext = context;
        sharedPreference = context.getSharedPreferences("Shared_Pre", Context.MODE_PRIVATE);
        editor = sharedPreference.edit();
    }

    public static SharedPreference_main getInstance(Context ctx) {
        if (sharedPreference_main == null) {
            sharedPreference_main = new SharedPreference_main(ctx);
            return sharedPreference_main;
        }
        return sharedPreference_main;
    }

    public void removePreference() {
        editor = sharedPreference.edit();
        editor.clear().apply();
    }

    public void setIs_LoggedIn(boolean Is_LoggedIn) {
        editor = sharedPreference.edit();
        editor.putBoolean("Is_LoggedIn", Is_LoggedIn);
        editor.commit();
    }

    public boolean getIs_LoggedIn() {
        return sharedPreference.getBoolean("Is_LoggedIn", false);
    }

    public void setUsername(String username) {
        editor = sharedPreference.edit();
        editor.putString("username", username);
        editor.commit();
    }


    public String getUsername() {
        return sharedPreference.getString("username", "");
    }


    public void setUserEmail(String Email) {
        editor = sharedPreference.edit();
        editor.putString("Email", Email);
        editor.commit();
    }

    public String getUserEmail() {
        return sharedPreference.getString("Email", "");
    }


    public void setUserId(String Id) {
        editor = sharedPreference.edit();
        editor.putString("Id", Id);
        editor.commit();
    }

    public String getUserId() {
        return sharedPreference.getString("Id", "");
    }


    public void setToken(String Token) {
        editor = sharedPreference.edit();
        editor.putString("Token", Token);
        editor.commit();
    }

    public String getToken() {
        return sharedPreference.getString("Token", "");
    }


    public void setUserImage(String Image) {
        editor = sharedPreference.edit();
        editor.putString("Image", Image);
        editor.commit();
    }

    public String getUserImage() {
        return sharedPreference.getString("Image", "");
    }

    public void setUserType(String UserType) {
        editor = sharedPreference.edit();
        editor.putString("UserType", UserType);
        editor.commit();
    }

    public String getUserType() {
        return sharedPreference.getString("UserType", "");
    }

    // setDAta for signUP

    public void setname(String name) {
        editor = sharedPreference.edit();
        editor.putString("name", name);
        editor.commit();
    }

    public String getname() {
        return sharedPreference.getString("name", "");
    }

    public void setemail(String email) {
        editor = sharedPreference.edit();
        editor.putString("email", email);
        editor.commit();
    }

    public String getemail() {
        return sharedPreference.getString("email", "");
    }

    public void setmobile(String mobile) {
        editor = sharedPreference.edit();
        editor.putString("mobile", mobile);
        editor.commit();
    }

    public String getmobile() {
        return sharedPreference.getString("mobile", "");
    }

    public void setdob(String dob) {
        editor = sharedPreference.edit();
        editor.putString("dob", dob);
        editor.commit();
    }

    public String getdob() {
        return sharedPreference.getString("dob", "");
    }


    public void seteducation(String education) {
        editor = sharedPreference.edit();
        editor.putString("education", education);
        editor.commit();
    }

    public String geteducation() {
        return sharedPreference.getString("education", "");
    }

    public void setaddress(String address) {
        editor = sharedPreference.edit();
        editor.putString("address", address);
        editor.commit();
    }

    public String getaddress() {
        return sharedPreference.getString("address", "");
    }


}
