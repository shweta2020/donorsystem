package com.nusys.donorsystem.commonModule;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;


import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.activity.About;
import com.nusys.donorsystem.activity.Blog;
import com.nusys.donorsystem.activity.ChangePassword;
import com.nusys.donorsystem.activity.ContactUs;
import com.nusys.donorsystem.activity.Login;
import com.nusys.donorsystem.activity.MainActivity;
import com.nusys.donorsystem.activity.Profile;
import com.nusys.donorsystem.activity.Services;
import com.nusys.donorsystem.model.ProfileModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;

/**
 * Created By: Shweta Agarwal
 * for side navigation
 * Created DAte:01-07-2020
 **/

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawerLayout;
    Toolbar toolbar;

    public FrameLayout frameLayout;
    NavigationView navigationView;
    SharedPreference_main sharedPreference_main;
    ImageView userLoginImg;
    TextView userName, userEmail;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_navigation);
        init();
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);

        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        sharedPreference_main = SharedPreference_main.getInstance(getApplicationContext());
        frameLayout = findViewById(R.id.content_frame);
        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);


        View header = navigationView.getHeaderView(0);
        userLoginImg = header.findViewById(R.id.userLoginImg);
        userName = header.findViewById(R.id.userName);
        userEmail = header.findViewById(R.id.userEmail);
        navigationView.setNavigationItemSelectedListener(this);

        //loadProfileData();

        userName.setText(sharedPreference_main.getUsername());
        userEmail.setText(sharedPreference_main.getUserEmail());

        Glide.with(getApplicationContext())
                .load(sharedPreference_main.getUserImage())
                .placeholder(R.drawable.user_icon)
                .into(userLoginImg);

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadProfileData();
    }

    private void loadProfileData() {

        HashMap<String, String> map = new HashMap<>();

        map.put("user_id", sharedPreference_main.getUserId());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<ProfileModel> call = serviceInterface.profile("Bearer " + sharedPreference_main.getToken(), CONTENT_TYPE, map);
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, retrofit2.Response<ProfileModel> response) {

                if (response.isSuccessful()) {

                    ProfileModel bean = response.body();
                    if (bean.getStatus() == 200) {


                        Glide.with(getApplicationContext())
                                .load(bean.getData().get(0).getProfilePic())
                                .placeholder(R.drawable.user_icon)
                                .into(userLoginImg);


                    } else {

                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                Log.e("error", t.getMessage());

            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.isChecked()) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }
        switch (item.getItemId()) {
            case R.id.nav_Home: {

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;
            }
            case R.id.nav_profile: {

                Intent intent = new Intent(getApplicationContext(), Profile.class);
                startActivity(intent);
                break;
            }
            case R.id.nav_about: {

                startActivity(new Intent(getApplicationContext(), About.class));
                break;
            }

            case R.id.nav_services: {

                startActivity(new Intent(getApplicationContext(), Services.class));
                break;
            }
            case R.id.nav_blog: {

                startActivity(new Intent(getApplicationContext(), Blog.class));
                break;
            }
            case R.id.nav_contact_us: {

                startActivity(new Intent(getApplicationContext(), ContactUs.class));
                break;
            }
            case R.id.nav_ChangePassword: {

                Intent intent = new Intent(getApplicationContext(), ChangePassword.class);
                startActivity(intent);

                break;
            }

            case R.id.nav_LogOut: {
                sharedPreference_main.removePreference();
                sharedPreference_main.setIs_LoggedIn(false);
                startActivity(new Intent(getApplicationContext(), Login.class));
                finish();

                break;
            }
            default:
                drawerLayout.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
