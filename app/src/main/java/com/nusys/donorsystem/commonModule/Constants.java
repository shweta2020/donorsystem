package com.nusys.donorsystem.commonModule;

/**
 * Created By: Shweta Agarwal
 * Created DAte:01-07-2020
 **/
public class Constants {

    public static final String BASE_URL = "http://13.233.62.88/donorsystem/api/web_api/";


    public static final String CONTENT_TYPE = "application/json";

    public static final String LOGIN = "login";//retrofit
    public static final String SIGNUP = "registration";//volley
    public static final String FORGOT_PWD = "forgot_password";//volley

    public static final String CASE_CATEGORY = "get_category";//retrofit
    public static final String COUNTRY_LIST = "get_country";//retrofit
    public static final String STATE_LIST = "get_state";//retrofit
    public static final String CITY_LIST = "get_city";//retrofit

    public static final String USER_TYPE = "get_usertype";//retrofit

    public static final String PROFILE = "profile";//retrofit
    public static final String UPDATE_PROFILE = "update_profile";//volley
    public static final String PROFILE_PIC_UPDATE = "update_profile_pic";//volley

    public static final String OPEN_CASE = "get_opencase";//retrofit
    public static final String ACTIVE_CASE_DONOR = "donor_active_case";//retrofit
    public static final String ACTIVE_CASE_RECEIVER = "reciever_case";//retrofit
    public static final String ADD_CASE = "add_case";//volley
    public static final String UPDATE_CASE = "update_case";//volley
    public static final String CASE_BY_ID = "case_by_id";//retrofit
    public static final String CASE_DELETE = "delete_case";//volley
    public static final String KYC = "KYC";//volley
    public static final String GET_CHAT = "get_chat_list";//volley
    public static final String ADD_CHAT = "add_chat";//volley
    public static final String ADD_CASE_CHAT = "add_case_chat";//volley

    public static final String GET_TRANSACTION = "get_transaction";//retrofit

    public static final String ABOUT = "get_about";//retrofit
    public static final String CONTACT_US = "get_contact_us";//retrofit
    public static final String SERVICES = "get_services";//retrofit
    public static final String BLOG = "get_blog";//retrofit
    public static final String CHANGE_PWD = "change_password";//volley

    public static final int GALLERY_REQUEST_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;

    public static final int GALLERY_REQUEST_CODE2 = 201;
    public static final int CAMERA_REQUEST_CODE2 = 202;

    public static final int GALLERY_REQUEST_CODE3 = 301;
    public static final int CAMERA_REQUEST_CODE3 = 302;

    public static final int GALLERY_REQUEST_CODE4 = 401;
    public static final int CAMERA_REQUEST_CODE4 = 402;

    public static final int GALLERY_REQUEST_CODE5 = 501;
    public static final int CAMERA_REQUEST_CODE5 = 502;

    public static final int GALLERY_REQUEST_CODE6 = 601;
    public static final int CAMERA_REQUEST_CODE6 = 602;

    public static final int GALLERY_REQUEST_CODE7 = 701;
    public static final int CAMERA_REQUEST_CODE7 = 702;

    public static final int GALLERY_REQUEST_CODE8 = 801;
    public static final int CAMERA_REQUEST_CODE8 = 802;

    public static final int GALLERY_REQUEST_CODE9 = 901;
    public static final int CAMERA_REQUEST_CODE9 = 902;

    public static final int GALLERY_REQUEST_CODE10 = 1001;
    public static final int CAMERA_REQUEST_CODE10 = 1002;

}
