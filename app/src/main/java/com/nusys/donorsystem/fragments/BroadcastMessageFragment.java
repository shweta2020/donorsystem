package com.nusys.donorsystem.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nusys.donorsystem.R;

/**
 * Created By: Shweta Agarwal
 * Created DAte:06-07-2020
 **/
public class BroadcastMessageFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater().inflate(R.layout.activity_broadcast_message_fragment, container, false);
        initialization(view);
        listener();
        return view;
    }

    private void initialization(View view) {

    }

    private void listener() {

    }
}
