package com.nusys.donorsystem.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.activity.CreateCase;
import com.nusys.donorsystem.adapter.AdapterActiveCaseR;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.ActiveCaseRModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:06-07-2020
 * Updated Date:10-07-2020(add adapter and api)
 **/
public class RActiveCaseFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout refreshLayout;

    ProgressBar mainProgress;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;

    SharedPreference_main sharedPreference_main;
    RecyclerView rvActivecaseList;
    AdapterActiveCaseR Adapter;
    private ProgressDialog pd;
    FloatingActionButton fabAdd;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater().inflate(R.layout.activity_r_active_case_fragment, container, false);
        initialization(view);
        listener();
        return view;
    }

    private void initialization(View view) {

        sharedPreference_main = SharedPreference_main.getInstance(getActivity());

        pd = new DialogProgress(getContext(), "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        refreshLayout = view.findViewById(R.id.refresh_layout);
        mainProgress = view.findViewById(R.id.main_progress);
        errorLayout = view.findViewById(R.id.error_layout);
        errorTxtCause = view.findViewById(R.id.error_txt_cause);
        errorBtnRetry = view.findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        rvActivecaseList = view.findViewById(R.id.rv_activecaseList);
        fabAdd = view.findViewById(R.id.fab_add);

    }

    private void listener() {
        /**
         * create case intent when click on floating action button
         * Created Date: 13-07-2020
         **/

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CreateCase.class));
            }
        });

    }

    /**
     * method for showing active case receiver
     * Created Date: 10-07-2020
     **/
    private void active_case_rec() {
        if (NetworkUtil.isConnected(getActivity())) {

            pd.show();
            HashMap<String, String> map = new HashMap<>();

            map.put("reciever_id", sharedPreference_main.getUserId());

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ActiveCaseRModel> call = serviceInterface.active_case_receiver(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<ActiveCaseRModel>() {


                @Override
                public void onResponse(Call<ActiveCaseRModel> call, retrofit2.Response<ActiveCaseRModel> response) {

                    if (response.isSuccessful()) {

                        ActiveCaseRModel bean = response.body();

                        if (bean.getStatus() == 200) {
                            ActiveCaseRModel view = response.body();
                            errorLayout.setVisibility(View.GONE);
                            rvActivecaseList.setVisibility(View.VISIBLE);

                            Adapter = new AdapterActiveCaseR(getContext(), bean.getData());
                            rvActivecaseList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            rvActivecaseList.setItemAnimator(new DefaultItemAnimator());
                            rvActivecaseList.setAdapter(Adapter);
                            Log.e("Group_response", view.toString());
                            pd.dismiss();
                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            rvActivecaseList.setVisibility(View.GONE);
                            Toast.makeText(getContext(), bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        rvActivecaseList.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ActiveCaseRModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    rvActivecaseList.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(getActivity(), new Dialog(getContext()));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                active_case_rec();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);

    }

    /*
     * add active_case_rec method on onstart()
     * Created Date:15-07-2020
     */
    @Override
    public void onStart() {
        super.onStart();
        /**calling method for showing active case receiver
         * Created Date: 10-07-2020
         **/
        active_case_rec();
    }
}