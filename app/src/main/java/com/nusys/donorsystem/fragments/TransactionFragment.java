package com.nusys.donorsystem.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.donorsystem.R;
import com.nusys.donorsystem.adapter.AdapterTransaction;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.TransactionModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:16-07-2020
 * Updated Date:20-07-2020(add apis and functionality)
 **/
public class TransactionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout refreshLayout;

    ProgressBar mainProgress;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;

    SharedPreference_main sharedPreference_main;
    RecyclerView rvTransactionList;
    AdapterTransaction Adapter;
    private ProgressDialog pd;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getLayoutInflater().inflate(R.layout.activity_transaction_fragment, container, false);
        initialization(view);
        listener();
        return view;
    }

    private void initialization(View view) {
        sharedPreference_main = SharedPreference_main.getInstance(getActivity());

        pd = new DialogProgress(getContext(), "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        refreshLayout = view.findViewById(R.id.refresh_layout);
        mainProgress = view.findViewById(R.id.main_progress);
        errorLayout = view.findViewById(R.id.error_layout);
        errorTxtCause = view.findViewById(R.id.error_txt_cause);
        errorBtnRetry = view.findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        rvTransactionList = view.findViewById(R.id.rv_transactionList);
    }

    private void listener() {
        /**calling method for showing active case receiver
         * Created Date: 16-07-2020
         * Updated Date:21-07-2020( remove donor transaction api, add common api for donor and receiver)
         **/
        get_transaction(sharedPreference_main.getUserType());
    }

    /**
     * method for showing transaction list
     * Created Date: 16-07-2020
     * Updated Date:21-07-2020( remove donor transaction api, add common api for donor and receiver)
     *
     * @param userType
     **/
    private void get_transaction(String userType) {
        if (NetworkUtil.isConnected(getActivity())) {

            pd.show();
            HashMap<String, String> map = new HashMap<>();

            map.put("user_id", sharedPreference_main.getUserId());
            map.put("user_typeid", userType);

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<TransactionModel> call = serviceInterface.get_transaction(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<TransactionModel>() {


                @Override
                public void onResponse(Call<TransactionModel> call, retrofit2.Response<TransactionModel> response) {

                    if (response.isSuccessful()) {

                        TransactionModel bean = response.body();

                        if (bean.getStatus() == 200) {
                            TransactionModel view = response.body();
                            errorLayout.setVisibility(View.GONE);
                            rvTransactionList.setVisibility(View.VISIBLE);

                            Adapter = new AdapterTransaction(getContext(), bean.getData());
                            rvTransactionList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            rvTransactionList.setItemAnimator(new DefaultItemAnimator());
                            rvTransactionList.setAdapter(Adapter);
                            Log.e("Group_response", view.toString());
                            pd.dismiss();
                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            rvTransactionList.setVisibility(View.GONE);
                            Toast.makeText(getContext(), bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        rvTransactionList.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TransactionModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    rvTransactionList.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(getActivity(), new Dialog(getContext()));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                get_transaction(sharedPreference_main.getUserType());
                refreshLayout.setRefreshing(false);
            }
        }, 2000);

    }

}