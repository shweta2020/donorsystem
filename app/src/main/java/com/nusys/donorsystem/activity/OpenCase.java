package com.nusys.donorsystem.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.adapter.AdapterChatComment;
import com.nusys.donorsystem.adapter.AdapterOpenCase;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.interfaces.GetCaseIds;
import com.nusys.donorsystem.model.GetChatModel;
import com.nusys.donorsystem.model.OpenCaseModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.donorsystem.commonModule.Constants.ADD_CASE_CHAT;
import static com.nusys.donorsystem.commonModule.Constants.ADD_CHAT;
import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.GET_CHAT;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:06-07-2020
 **/
public class OpenCase extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, GetCaseIds {
    SwipeRefreshLayout refreshLayout;
    LinearLayout llBackActivity;
    TextView tvToolbarHead;

    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    private ProgressDialog pd;

    SharedPreference_main sharedPreference_main;
    Context context;
    RecyclerView rvActivecaseList;
    AdapterOpenCase Adapter;


    Dialog dialog;
    RecyclerView rvComment;
    LinearLayout llNoComment;
    EditText etComment;
    ImageView ivOpenGalley, ivSend;
    private List<GetChatModel> commentList;
    AdapterChatComment myAdapterComment;
    private String userImageBase64 = "";
    String etChatComment = "";
    String msgType;
    String case_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_case);
        initialization();
        action();
    }

    public void initialization() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        context = OpenCase.this;
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        refreshLayout = findViewById(R.id.refresh_layout);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        rvActivecaseList = findViewById(R.id.rv_activecaseList);
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_chat);
        rvComment = dialog.findViewById(R.id.rv_comment);
        llNoComment = dialog.findViewById(R.id.ll_noComment);
        etComment = dialog.findViewById(R.id.et_comment);
        ivOpenGalley = dialog.findViewById(R.id.iv_open_galley);
        ivSend = dialog.findViewById(R.id.iv_send);

    }

    public void action() {

        /* finish activity when click on bake arrow
         * Created Date: 07-07-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /*method calling for fetching open cases
         * Created Date: 06-07-2020
         * Updated Date:
         */
        open_case();


    }

    /*method calling for fetching open cases
     * Created Date: 06-07-2020
     * Updated Date: 07-07-2020(change api name)
     */
    private void open_case() {
        if (NetworkUtil.isConnected(context)) {

            pd.show();
            HashMap<String, String> map = new HashMap<>();

            map.put("category_id", getIntent().getStringExtra("cate_id"));

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<OpenCaseModel> call = serviceInterface.open_case(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<OpenCaseModel>() {


                @Override
                public void onResponse(Call<OpenCaseModel> call, retrofit2.Response<OpenCaseModel> response) {

                    if (response.isSuccessful()) {

                        OpenCaseModel bean = response.body();

                        //Toast.makeText(batch_Activity.this, "sucess", Toast.LENGTH_SHORT).show();
                        if (bean.getStatus() == 200) {

                            tvToolbarHead.setText(bean.getData().get(0).getCaseCategory());

                            errorLayout.setVisibility(View.GONE);
                            rvActivecaseList.setVisibility(View.VISIBLE);

                            Adapter = new AdapterOpenCase(context, bean.getData(), OpenCase.this);
                            rvActivecaseList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            rvActivecaseList.setItemAnimator(new DefaultItemAnimator());
                            rvActivecaseList.setAdapter(Adapter);
                            Log.e("Group_response", bean.toString());
                            pd.dismiss();
                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            rvActivecaseList.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        rvActivecaseList.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OpenCaseModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    rvActivecaseList.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                open_case();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);

    }

    /*fatching ids for chat
     * Created Date: 20-07-2020
     */
    @Override
    public void getCaseIds(String caseId) {
        this.case_id = caseId;
        /*open dialog box for chat
         * Created Date: 20-07-2020
         */

        dialog.show();
        /* set dialog box height and width
         *  Created Date:20-07-2020
         */

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;
        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        int dialogWindowWidth = (int) (displayWidth * 0.9f);
        int dialogWindowHeight = (int) (displayHeight * 0.8f);
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = dialogWindowHeight;
        // Apply the newly created layout parameters to the alert dialog window
        dialog.getWindow().setAttributes(layoutParams);
        /*calling method for fetching chat
         * Created Date: 20-07-2020
         */
        getComments();
        /*method for open camera
         * Created Date: 20-07-2020
         */

        ivOpenGalley.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);
            }
        });
        /*send chat when click on ivsend
         * Created Date: 20-07-2020
         */
        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_chat_case();
                if (!userImageBase64.equals("")) {
                    DoComment(etChatComment);
                } else {
                    if (TextUtils.isEmpty(etComment.getText().toString())) {
                        etComment.setError("field can't be empty");
                    } else {
                        DoComment(etComment.getText().toString());
                    }
                }
            }
        });
    }

    /*method for add chat in case(update case id)
     * Created Date: 20-07-2020
     */
    private void add_chat_case() {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        final JSONObject jsonBody = new JSONObject();
        try {


            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("case_id", getIntent().getStringExtra("cate_id"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ADD_CASE_CHAT, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Integer status = response.getInt("status");

                            if (status == 200) {

                                //  Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(context, "error" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
    /*method for get chat
     * Created Date: 20-07-2020
     */

    private void getComments() {

        HashMap<String, String> map = new HashMap<>();

        map.put("user_id", sharedPreference_main.getUserId());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<GetChatModel> call = serviceInterface.get_chat(sharedPreference_main.getToken(), CONTENT_TYPE, map);
        call.enqueue(new Callback<GetChatModel>() {


            @Override
            public void onResponse(Call<GetChatModel> call, retrofit2.Response<GetChatModel> response) {

                if (response.isSuccessful()) {

                    GetChatModel bean = response.body();

                    if (bean.getStatus() == 200) {
                        GetChatModel view = response.body();


                        myAdapterComment = new AdapterChatComment(context, bean.getData());
                        rvComment.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        rvComment.setItemAnimator(new DefaultItemAnimator());
                        rvComment.setAdapter(myAdapterComment);
                        Log.e("Group_response", view.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetChatModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*method for do chat
     * Created Date: 20-07-2020
     */
    private void DoComment(String etChatComment) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        final JSONObject jsonBody = new JSONObject();
        try {


            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("msg", etChatComment);
            jsonBody.put("image", userImageBase64);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ADD_CHAT, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Integer status = response.getInt("status");

                            if (status == 200) {
                                etComment.setText("");
                                userImageBase64 = "";
                                ivOpenGalley.setImageResource(R.drawable.ic_photo_camera_black_24dp);
                                getComments();
                                //  Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(context, "error" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
    /*method for open gallery and camera
     * Created Date: 20-07-2020
     */

    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
//                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);
//
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivOpenGalley.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    // printLog(userImageBase64);
                    if (userImageBase64.equals("")) {
                        msgType = "text";
                    } else {
                        msgType = "image";
                    }
//                    tv_userImage.setText(getCacheImagePath(fileName).toString());

//                  getCacheImagePath(fileName);
                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivOpenGalley.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);
                        if (userImageBase64.equals("")) {
                            msgType = "text";
                        } else {
                            msgType = "image";
                        }

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());
                break;


        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        //  printLog(imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.chooser);
        TextView camera = dialog.findViewById(R.id.camera);
        TextView gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }
}
