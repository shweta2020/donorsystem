package com.nusys.donorsystem.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.Extension;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created By: Shweta Agarwal
 * Created DAte:30-06-2020
 **/
public class Splash extends AppCompatActivity {

    SharedPreference_main sharedPreference_main;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        sharedPreference_main = SharedPreference_main.getInstance(this);
        dialog = new Dialog(this);
        //for splash shows in full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // date validation for apk
        Date currentdate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String str2 = "30/08/2020";
        try {
            Date date2 = formatter.parse(str2);

            if (currentdate.after(date2)) {
                Extension.showErrorDialog(this, dialog);
                //finish();
            } else {
                if (NetworkUtil.isConnected(this)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // This method will be executed once the timer is over
                            if (sharedPreference_main.getIs_LoggedIn()) {
                                startActivity(new Intent(Splash.this, MainActivity.class));
                                finish();
                            } else {
                                Intent i = new Intent(Splash.this, Login.class);
                                startActivity(i);
                                finish();
                            }
                        }
                    }, 1000);
                } else {
                    Extension.showErrorDialog(this, dialog);
//            showPopup();
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
