package com.nusys.donorsystem.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.BaseActivity;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.CONTACT_US;
import static com.nusys.donorsystem.commonModule.Constants.SIGNUP;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:01-07-2020
 * Updated Date:21-07-2020(add error layout and swipe refresh, progress bar)
 **/

public class ContactUs extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout refreshLayout;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    private ProgressDialog pd;

    Context context;
    SharedPreference_main sharedPreference_main;
    LinearLayout llBackActivity;
    TextView tvToolbarHead;

    TextView tvPhoneNum, tvEmail, tvAddress, tvWebsite;
    View viewColor;
    LinearLayout llMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        initialization();
        action();
    }

    public void initialization() {
        context = ContactUs.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        refreshLayout = findViewById(R.id.refresh_layout);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        llBackActivity = findViewById(R.id.ll_back_activity);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);
        tvPhoneNum = findViewById(R.id.tv_phone_num);
        tvEmail = findViewById(R.id.tv_email);
        tvAddress = findViewById(R.id.tv_address);
        tvWebsite = findViewById(R.id.tv_website);
        viewColor = findViewById(R.id.view_color);
        llMain = findViewById(R.id.ll_main);
    }

    public void action() {
        /* finish activity when click on bake arrow
         * Created Date: 01-07-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /* call fetch contact us method
         * Created Date: 04-07-2020
         */
        contact_us();
    }

    /* method for contact us
     * Created Date: 04-07-2020
     * Updated Date: 04-07-2020(add field website)
     * Updated Date: 21-07-2020(add error view when network not connected)
     */
    private void contact_us() {

        if (NetworkUtil.isConnected(context)) {

            pd.show();

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            JSONObject jsonBody = new JSONObject();

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, BASE_URL + CONTACT_US, jsonBody,
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Integer status = response.getInt("status");
                                if (status == 200) {
                                    //     pd.dismiss();
                                    JSONArray jsonArray = response.getJSONArray("data");
                                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                                    tvPhoneNum.setText(jsonObject.getString("contact"));
                                    tvEmail.setText(jsonObject.getString("email"));
                                    tvAddress.setText(Html.fromHtml(jsonObject.getString("address")).toString());
                                    tvWebsite.setText(jsonObject.getString("website"));
                                    pd.dismiss();
                                } else {
                                    pd.dismiss();
                                    errorLayout.setVisibility(View.VISIBLE);
                                    llMain.setVisibility(View.GONE);
                                    viewColor.setVisibility(View.GONE);
                                    Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                pd.dismiss();
                                errorLayout.setVisibility(View.VISIBLE);
                                llMain.setVisibility(View.GONE);
                                viewColor.setVisibility(View.GONE);
                                e.printStackTrace();
                            }
                            Log.e("response1", response.toString());

                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    llMain.setVisibility(View.GONE);
                    viewColor.setVisibility(View.GONE);
                }
            });
            requestQueue.add(jsonObjectRequest);

        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                contact_us();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);
    }
}
