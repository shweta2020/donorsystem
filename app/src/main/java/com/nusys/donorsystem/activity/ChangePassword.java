package com.nusys.donorsystem.activity;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.BaseActivity;
import com.nusys.donorsystem.commonModule.SharedPreference_main;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.CHANGE_PWD;

/**
 * Created By: Shweta Agarwal
 * Created DAte:01-07-2020
 * Updated Date:06-07-2020(add method for change password and validation )
 **/
public class ChangePassword extends BaseActivity {
    Context context;
    SharedPreference_main sharedPreference_main;

    Button btn_submit;
    EditText st_old_password, st_confirm_password, st_new_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_change_password, frameLayout);
        setTitle("Change Password");

        initialization();
        action();
    }

    public void initialization() {
        context = ChangePassword.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        btn_submit = findViewById(R.id.btSubmit);
        st_confirm_password = findViewById(R.id.stConfirmPassword);
        st_old_password = findViewById(R.id.et_oldPassword);
        st_new_password = findViewById(R.id.stNewPassword);
    }

    public void action() {

        /*calling method for change password
         * Created Date: 06-07-2020
         * Updated Date:
         */
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(st_old_password.getText().toString())) {
                    st_old_password.setError("field can't be empty");
                } else if (TextUtils.isEmpty(st_new_password.getText().toString())) {
                    st_new_password.setError("field can't be empty");
                } else if (TextUtils.isEmpty(st_confirm_password.getText().toString())) {
                    st_confirm_password.setError("field can't be empty");
                } else if (!st_new_password.getText().toString().equals(st_confirm_password.getText().toString())) {
                    st_confirm_password.setError("confirm password not matched");

                } else {

                    post_Json_Request();
                }

            }
        });
    }

    private void post_Json_Request() {


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            //input your API parameters
            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("oldpass", st_old_password.getText().toString());
            jsonBody.put("newpass", st_new_password.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + CHANGE_PWD, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            Integer status = response.getInt("status");
                            if (status == 200) {
                                startActivity(new Intent(ChangePassword.this, Login.class));
                                finish();
                                Toast.makeText(ChangePassword.this, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ChangePassword.this, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
