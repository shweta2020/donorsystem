package com.nusys.donorsystem.activity;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.donorsystem.R;
import com.nusys.donorsystem.adapter.AdapterService;
import com.nusys.donorsystem.commonModule.BaseActivity;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.ServiceModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:02-07-2020
 * Updated Date:07-07-2020(apply functionality, methods, recycler etc.)
 **/
public class Services extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context context;
    SharedPreference_main sharedPreference_main;

    SwipeRefreshLayout refreshLayout;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    private ProgressDialog pd;

    RecyclerView rvActivecaseList;
    AdapterService Adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_services, frameLayout);
        setTitle("Services");

        initialization();
        action();


    }

    public void initialization() {
        context = Services.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        refreshLayout = findViewById(R.id.refresh_layout);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        rvActivecaseList = findViewById(R.id.rv_activecaseList);
    }

    public void action() {

        /* call fetch about us method
         * Created Date: 07-07-2020
         */
        fetch_about_us();
    }

    private void fetch_about_us() {
        if (NetworkUtil.isConnected(context)) {
            pd.show();

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ServiceModel> call = serviceInterface.fetch_services();
            call.enqueue(new Callback<ServiceModel>() {


                @Override
                public void onResponse(Call<ServiceModel> call, retrofit2.Response<ServiceModel> response) {

                    if (response.isSuccessful()) {

                        ServiceModel bean = response.body();

                        if (bean.getStatus() == 200) {

                            errorLayout.setVisibility(View.GONE);
                            rvActivecaseList.setVisibility(View.VISIBLE);

                            Adapter = new AdapterService(context, bean.getData());
                            rvActivecaseList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            rvActivecaseList.setItemAnimator(new DefaultItemAnimator());
                            rvActivecaseList.setAdapter(Adapter);
                            Log.e("Group_response", bean.toString());
                            pd.dismiss();
                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            rvActivecaseList.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        rvActivecaseList.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ServiceModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    rvActivecaseList.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetch_about_us();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);

    }
}
