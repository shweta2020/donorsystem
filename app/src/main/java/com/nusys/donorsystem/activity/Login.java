package com.nusys.donorsystem.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.LoginModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created By: Shweta Agarwal
 * Created DAte:30-06-2020
 **/
public class Login extends AppCompatActivity {

    Context context;
    SharedPreference_main sharedPreference_main;
    TextView tvForgotPassword;
    EditText etEmail, etPassword;
    Button btLogin, btSignup;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialization();
        action();

    }

    public void initialization() {
        context = Login.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);

        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);

        tvForgotPassword = findViewById(R.id.tv_forgot_pwd);
        btLogin = findViewById(R.id.bt_login);
        btSignup = findViewById(R.id.bt_sign_up);

    }

    public void action() {
        /*redirect to register page when click on btSignup
         * Created Date: 30-06-2020
         */
        btSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SignUp.class);
                startActivity(i);
            }
        });
        /*redirect to home page when click on btLogin with validation and calling dologin method
         * Created Date: 30-06-2020
         * Updated Date: 01-07-2020(apply do login api)
         *  Updated Date:14-07-2020(add email pattern validation)
         */
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(etEmail.getText().toString())) {

                    etEmail.setError("field can't be empty");

                } else if (!etEmail.getText().toString().matches(emailPattern)) {
                    Toast.makeText(context, "Please Enter The Correct Email", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty((etPassword.getText().toString()))) {

                    etPassword.setError("field can't be empty");
                } else {
                    doLogin();

                }
            }
        });
        /*redirect to forgot password page when click on tvForgotPassword
         * Created Date: 30-06-2020
         */
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ForgotPassword.class);
                startActivity(i);
            }
        });
    }

    /*method for doing login
     * Created Date: 30-06-2020
     * Updated Date: 01-07-2020( add some more values in sharedPreference_main)
     * Updated Date: 02-07-2020( combine firstname & middlename to username)
     */
    private void doLogin() {

        HashMap<String, String> map = new HashMap<>();
        map.put("user_name", etEmail.getText().toString());
        map.put("password", etPassword.getText().toString());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<LoginModel> call = serviceInterface.doLogin(map);
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.isSuccessful()) {
                    LoginModel bean = response.body();
                    if (bean.getStatus().equals(200)) {

                        sharedPreference_main.setIs_LoggedIn(true);
                        sharedPreference_main.setUserId(bean.getData().get(0).getUserId());
                        sharedPreference_main.setUsername(bean.getData().get(0).getUsername());
                        sharedPreference_main.setUserEmail(bean.getData().get(0).getEmail());
                        sharedPreference_main.setToken(bean.getToken());
                        sharedPreference_main.setUserImage(bean.getData().get(0).getProfilePic());
                        sharedPreference_main.setUserType(bean.getData().get(0).getUserTypeid());
                        Intent i = new Intent(context, MainActivity.class);
                        startActivity(i);
                        Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Something is wrong", Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();

            }
        });


    }
}
