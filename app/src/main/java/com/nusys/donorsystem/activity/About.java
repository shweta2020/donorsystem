package com.nusys.donorsystem.activity;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.BaseActivity;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.AboutModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;
import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:01-07-2020
 * Updated Date:21-07-2020(add error layout and swipe refresh, progress bar)
 **/
public class About extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    SwipeRefreshLayout refreshLayout;

    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;

    Context context;
    SharedPreference_main sharedPreference_main;
    TextView tvAboutUs, tvTitle;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_about, frameLayout);
        setTitle("About Us");

        initialization();
        action();
    }

    public void initialization() {
        context = About.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        refreshLayout = findViewById(R.id.refresh_layout);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);


        tvAboutUs = findViewById(R.id.tv_about_us);
        tvTitle = findViewById(R.id.tv_title);
    }

    @SuppressLint("WrongConstant")
    public void action() {
        /* for justify text view and can add [android:justificationMode="inter_word" ] in xml
         * Created Date: 17-07-2020
         */

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            tvAboutUs.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        }
        /* call fetch about us method
         * Created Date: 01-07-2020
         */
        fetch_about_us();
    }

    /*  method for fetching privacy policy
     *  Created Date: 01-07-2020
     * Updated Date: 21-07-2020(add error view when network not connected)
     */
    private void fetch_about_us() {
        if (NetworkUtil.isConnected(context)) {

            pd.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<AboutModel> call = serviceInterface.fetch_about(CONTENT_TYPE);
            call.enqueue(new Callback<AboutModel>() {
                @Override
                public void onResponse(Call<AboutModel> call, Response<AboutModel> response) {
                    if (response.isSuccessful()) {
                        AboutModel bean = response.body();
                        if (bean.getStatus() == 200) {


                            tvTitle.setText(bean.getData().get(0).getTitle());

                            tvAboutUs.setText(Html.fromHtml(bean.getData().get(0).getDescription()).toString());
                            pd.dismiss();
                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        Toast.makeText(context, "Something is wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AboutModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    Toast.makeText(context, "Something is wrong", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetch_about_us();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);

    }
}
