package com.nusys.donorsystem.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.CityModel;
import com.nusys.donorsystem.model.CountryModel;
import com.nusys.donorsystem.model.LoginModel;
import com.nusys.donorsystem.model.StateModel;
import com.nusys.donorsystem.model.UserTypeModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Constants.SIGNUP;

/**
 * Created By: Shweta Agarwal
 * Created DAte:01-07-2020
 **/
public class SignUp extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;

    EditText etFirstName, etMiddleName, etMobileNum, etEmailId, etAddress1, etAddress2;
    Button btnSignUp;
    Spinner spCountry, spState, spCity, spUserType;

    String spCountryID, spCityID, spStateID, spUserTypeID;

    private ArrayList<String> spinCountry = new ArrayList<String>();
    private ArrayList<String> spinCountry_id = new ArrayList<String>();

    private ArrayList<String> spinState = new ArrayList<String>();
    private ArrayList<String> spinState_id = new ArrayList<String>();

    private ArrayList<String> spinCity = new ArrayList<String>();
    private ArrayList<String> spinCity_id = new ArrayList<String>();

    private ArrayList<String> spinUserType = new ArrayList<String>();
    private ArrayList<String> spUserType_id = new ArrayList<String>();

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initialization();
        action();
    }

    public void initialization() {
        context = SignUp.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);

        etFirstName = findViewById(R.id.et_first_name);
        etMiddleName = findViewById(R.id.et_middle_name);
        etMobileNum = findViewById(R.id.et_mobile_num);
        etEmailId = findViewById(R.id.et_email_id);
        etAddress1 = findViewById(R.id.et_address1);
        etAddress2 = findViewById(R.id.et_address2);
        spCountry = findViewById(R.id.sp_country);
        spState = findViewById(R.id.sp_state);
        spCity = findViewById(R.id.sp_city);
        spUserType = findViewById(R.id.sp_user_type);
        btnSignUp = findViewById(R.id.btn_sign_up);

        spinCountry.add("Select Country");
        spinCountry_id.add("-1");

        spinUserType.add("Select User Type");
        spUserType_id.add("-1");
    }

    public void action() {


        /*  action for taking spinner country value
         *  Created Date: 01-07-2020
         * Updated Date:02-07-2020(add view_city_list method)
         */

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spCountryID = spinCountry_id.get(position);
                view_state_list(spinCountry_id.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*  action for taking spinner State value
         *  Created Date: 02-07-2020
         * Updated Date:
         */
        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spStateID = spinState_id.get(position);

                view_city_list(spStateID);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*  action for taking spinner city value
         *  Created Date: 02-07-2020
         * Updated Date:
         */
        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spCityID = spinCity_id.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*  method for country list
         *  Created Date: 01-07-2020
         */
        view_country_list();
        /*  method for user type
         *  Created Date: 01-07-2020
         */
        view_usertype_list();

        /*  action for taking spinner type of user
         *  Created Date: 01-07-2020
         */

        spUserType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spUserTypeID = spUserType_id.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*  when click on signup button first check validation and then hit api
         *  Created Date: 01-07-2020
         *  Updated Date:14-07-2020(add email pattern validation)
         *  Updated Date:01-07-2020(calling method doSignup)
         *  Updated Date:22-07-2020(add spinner validation)
         */

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((TextUtils.isEmpty(etFirstName.getText().toString()))) {

                    etFirstName.setError("field can't be empty");

                } else if ((TextUtils.isEmpty(etMiddleName.getText().toString()))) {

                    etMiddleName.setError("field can't be empty");

                } else if (TextUtils.isEmpty(etEmailId.getText().toString())) {

                    etEmailId.setError("field can't be empty");

                } else if (!etEmailId.getText().toString().matches(emailPattern)) {

                    Toast.makeText(context, "Please Enter The Correct Email", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(etMobileNum.getText().toString())) {

                    etMobileNum.setError("field can't be empty");

                } else if (etMobileNum.getText().toString().length() < 10 || etMobileNum.getText().toString().length() > 13) {

                    Toast.makeText(context, "Please enter valid phone number", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(etAddress1.getText().toString())) {

                    etAddress1.setError("field can't be empty");

                } else if (TextUtils.isEmpty(etAddress2.getText().toString())) {

                    etAddress2.setError("field can't be empty");

                } else if (spCountryID.equals("-1")) {

                    Toast.makeText(context, "Please Select Country", Toast.LENGTH_SHORT).show();

                } else if (spStateID.equals("-1")) {

                    Toast.makeText(context, "Please Select State", Toast.LENGTH_SHORT).show();

                } else if (spCityID.equals("-1")) {

                    Toast.makeText(context, "Please Select City", Toast.LENGTH_SHORT).show();

                } else if (spUserTypeID.equals("-1")) {

                    Toast.makeText(context, "Please Select User Type", Toast.LENGTH_SHORT).show();

                } else {
                    do_signup();
                }
            }
        });

    }


    /*  method for fetching country list
     *  Created Date: 01-07-2020
     */

    private void view_country_list() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<CountryModel> call = serviceInterface.country_list(CONTENT_TYPE);
        call.enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(Call<CountryModel> call, retrofit2.Response<CountryModel> response) {
                if (response.isSuccessful()) {
                    CountryModel bean = response.body();
                    if (bean.getStatus() == 200) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String countryName = bean.getData().get(i).getCountryName();
                            String countryID = bean.getData().get(i).getCountryId();


                            spinCountry.add(countryName);
                            spinCountry_id.add(countryID);

                        }

                    } else {
                        Toast.makeText(context, "something is wrong", Toast.LENGTH_SHORT).show();
                    }
                    spCountry.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCountry));

                } else {
                    Toast.makeText(context, "Something is wrong when fetching country ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountryModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();

            }
        });


    }


    /*  method for fetching state list
     *  Created Date: 02-07-2020
     */

    private void view_state_list(String id_country) {
        spinState.clear();
        spinState_id.clear();
        spinState.add("Select State");
        spinState_id.add("-1");
        HashMap<String, String> map = new HashMap<>();
        map.put("country_id", id_country);

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<StateModel> call = serviceInterface.state_list(map);
        call.enqueue(new Callback<StateModel>() {
            @Override
            public void onResponse(Call<StateModel> call, Response<StateModel> response) {
                if (response.isSuccessful()) {
                    StateModel bean = response.body();
                    if (bean.getStatus().equals(200)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String stateName = bean.getData().get(i).getStateName();
                            String stateId = bean.getData().get(i).getStateId();


                            spinState.add(stateName);
                            spinState_id.add(stateId);
                            //    spin_pay_methd_id.add(paymentMethodId);
                        }
                        spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

                    } else {
                        spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

                        //    Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

                }
            }


            @Override
            public void onFailure(Call<StateModel> call, Throwable t) {
                spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

                // Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();

            }
        });


    }

    /*  method for fetching city list
     *  Created Date: 02-07-2020
     */

    private void view_city_list(String id_state) {
        spinCity.clear();
        spinCity_id.clear();
        spinCity.add("Select City");
        spinCity_id.add("-1");
        HashMap<String, String> map = new HashMap<>();
        map.put("state_id", id_state);

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<CityModel> call = serviceInterface.city_list(map);
        call.enqueue(new Callback<CityModel>() {
            @Override
            public void onResponse(Call<CityModel> call, Response<CityModel> response) {
                if (response.isSuccessful()) {
                    CityModel bean = response.body();
                    if (bean.getStatus().equals(200)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String cityName = bean.getData().get(i).getCityName();
                            String cityId = bean.getData().get(i).getCityId();


                            spinCity.add(cityName);
                            spinCity_id.add(cityId);

                        }
                        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                    } else {
                        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                    }

                } else {
                    spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                }
            }


            @Override
            public void onFailure(Call<CityModel> call, Throwable t) {
                spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

            }
        });


    }
    /*  method for fetching user type
     *  Created Date: 01-07-2020
     */

    private void view_usertype_list() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<UserTypeModel> call = serviceInterface.user_type(CONTENT_TYPE);
        call.enqueue(new Callback<UserTypeModel>() {
            @Override
            public void onResponse(Call<UserTypeModel> call, retrofit2.Response<UserTypeModel> response) {
                if (response.isSuccessful()) {
                    UserTypeModel bean = response.body();
                    if (bean.getStatus() == 200) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String UserType = bean.getData().get(i).getUserTypename();
                            String UserTypeID = bean.getData().get(i).getUserTypeid();

                            spinUserType.add(UserType);
                            spUserType_id.add(UserTypeID);

                        }


                    } else {
                        Toast.makeText(context, "something is wrong", Toast.LENGTH_SHORT).show();
                    }
                    spUserType.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinUserType));

                } else {
                    Toast.makeText(context, "Something is wrong when fetching usertype ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserTypeModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();

            }
        });


    }

    /*  method for register user
     *  Created Date: 01-07-2020
     */
    private void do_signup() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("firstname", etFirstName.getText().toString());
            jsonBody.put("middlename", etMiddleName.getText().toString());
            jsonBody.put("email", etEmailId.getText().toString());
            jsonBody.put("phone_no", etMobileNum.getText().toString());
            jsonBody.put("address1", etAddress1.getText().toString());
            jsonBody.put("address2", etAddress2.getText().toString());
            jsonBody.put("city_id", spCityID);
            jsonBody.put("state_id", spStateID);
            jsonBody.put("country_id", spCountryID);
            jsonBody.put("user_typeid", spUserTypeID);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + SIGNUP, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Integer status = response.getInt("status");
                            if (status == 200) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                Intent i = new Intent(context, Login.class);
                                startActivity(i);

                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonObjectRequest);

    }
}
