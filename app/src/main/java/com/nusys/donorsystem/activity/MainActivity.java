package com.nusys.donorsystem.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.animations.DescriptionAnimation;
import com.glide.slider.library.slidertypes.BaseSliderView;
import com.glide.slider.library.slidertypes.TextSliderView;
import com.glide.slider.library.tricks.ViewPagerEx;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.adapter.AdapterCaseType;
import com.nusys.donorsystem.commonModule.BaseActivity;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.fragments.DActiveCaseFragment;
import com.nusys.donorsystem.fragments.RActiveCaseFragment;
import com.nusys.donorsystem.fragments.BroadcastMessageFragment;
import com.nusys.donorsystem.fragments.TransactionFragment;
import com.nusys.donorsystem.model.CaseTypeModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:30-07-2020
 * Updated Date:22-07-2020(add error layout and  progress bar)
 **/
public class MainActivity extends BaseActivity implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    Context context;
    SharedPreference_main sharedPreference_main;
    private SliderLayout slLider;
    LinearLayout floating_layout;

    RecyclerView rvCaseType;
    AdapterCaseType Adapter;

    private ProgressDialog pd;
    LinearLayout linear_hit;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;

    FrameLayout frameContainer, mainFrame;
    BottomNavigationView navigation;
    FloatingActionButton fabChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_main, frameLayout);
        setTitle("Home");
        initialization();
        action();

    }

    public void initialization() {
        context = MainActivity.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);

        navigation = findViewById(R.id.navigation_home);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        floating_layout = findViewById(R.id.floating_layout_home);

        slLider = findViewById(R.id.slider);

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        linear_hit = findViewById(R.id.linearhit);

        rvCaseType = findViewById(R.id.rv_case_type);
        frameContainer = findViewById(R.id.frame_container_home);
        mainFrame = findViewById(R.id.main_frame);

        fabChat = findViewById(R.id.fab_chat);
    }

    public void action() {
        /**for slider
         * Created Date: 30-06-2020
         **/
        slider();
        /**for view case type
         * Created Date: 30-06-2020
         * Updated Date:01-07-2020(change post to get method)
         **/
        view_case_type();
        /**open chat box when click on floating point action button
         * Created Date: 20-07-2020
         * Updated Date:
         **/
        fabChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, Chat.class));
            }
        });
    }

    /*  method for slider
     *  Created Date:30-06-2020
     * Updated Date:
     */
    private void slider() {


        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();

        listUrl.add("https://static1.bigstockphoto.com/9/4/3/large1500/349463413.jpg");
        listName.add("JPG - Event");

        listUrl.add("https://ak5.picdn.net/shutterstock/videos/1035609005/thumb/1.jpg");
        listName.add("PNG - Event");

        listUrl.add("https://image.cnbcfm.com/api/v1/image/106563559-1591197652833gettyimages-1236237151.jpeg?v=1591197906&w=1400&h=950");
        listName.add("GIF - Event");

        listUrl.add("https://ak3.picdn.net/shutterstock/videos/10154213/thumb/1.jpg");
        listName.add("WEBP - Event");

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();


        for (int i = 0; i < listUrl.size(); i++) {
            TextSliderView sliderView = new TextSliderView(MainActivity.this);
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                    .image(listUrl.get(i))
                    .description(listName.get(i))
                    .setRequestOption(requestOptions)
                    .setProgressBarVisible(true)
                    .setOnSliderClickListener(this);

            //add your extra information
            sliderView.bundle(new Bundle());
            sliderView.getBundle().putString("extra", listName.get(i));
            slLider.addSlider(sliderView);
        }

        // set Slider Transition Animation
        // slLider.setPresetTransformer(SliderLayout.Transformer.Default);
        slLider.setPresetTransformer(SliderLayout.Transformer.Accordion);

        slLider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slLider.setCustomAnimation(new DescriptionAnimation());
        slLider.setDuration(4000);
        slLider.addOnPageChangeListener(this);
        slLider.stopCyclingWhenTouch(false);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /*  method for fetching cases
     *  Created Date: 30-06-2020
     * Updated Date: 22-07-2020(add error view when network not connected)
     */
    private void view_case_type() {
        if (NetworkUtil.isConnected(context)) {

            pd.show();
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<CaseTypeModel> call = serviceInterface.case_category(sharedPreference_main.getToken(), CONTENT_TYPE);
            call.enqueue(new Callback<CaseTypeModel>() {
                @Override
                public void onResponse(Call<CaseTypeModel> call, Response<CaseTypeModel> response) {
                    if (response.isSuccessful()) {
                        CaseTypeModel bean = response.body();
                        if (bean.getStatus() == 200) {


                            Adapter = new AdapterCaseType(context, bean.getData());
                            rvCaseType.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            rvCaseType.setItemAnimator(new DefaultItemAnimator());
                            rvCaseType.setAdapter(Adapter);
                            pd.dismiss();

                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            linear_hit.setVisibility(GONE);
                            Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        linear_hit.setVisibility(GONE);
                        Toast.makeText(context, "Something is wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CaseTypeModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    linear_hit.setVisibility(GONE);
                    Toast.makeText(context, "Something is wrongs", Toast.LENGTH_SHORT).show();

                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    /*for bottom navigation
     * Created Date: 10-03-2020
     * Updated Date:10-07-2020(add condition for open different active case page on diff user type)
     */

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent i = new Intent(context, MainActivity.class);
                    startActivity(i);

                    return true;

                case R.id.navigation_transaction:
                    setTitle("Transaction");
                    frameContainer.setVisibility(VISIBLE);
                    mainFrame.setVisibility(GONE);
                    floating_layout.setVisibility(View.GONE);
                    TransactionFragment transactionFragment = new TransactionFragment();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.add(R.id.frame_container_home, transactionFragment);
                    transaction.addToBackStack("transaction");
                    transaction.commit();
                    return true;

                case R.id.navigation_active_case:

                    if (sharedPreference_main.getUserId().equals("1")) {
                        fragment = new DActiveCaseFragment();
                    } else if (sharedPreference_main.getUserId().equals("2")) {
                        fragment = new RActiveCaseFragment();
                    } else {
                        fragment = new DActiveCaseFragment();
                    }

                    loadFragment(fragment);
                    floating_layout.setVisibility(View.GONE);
                    setTitle("Active Case");

                    return true;

                case R.id.navigation_broadcast:
                    floating_layout.setVisibility(View.GONE);
                    fragment = new BroadcastMessageFragment();
                    loadFragment(fragment);
                    setTitle("Notification");
                    // removeBadge();
                    return true;

            }

            return false;

        }
    };

    private void loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            mainFrame.setVisibility(GONE);
            frameContainer.setVisibility(VISIBLE);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container_home, fragment);
            transaction.addToBackStack("Tests");
            getSupportFragmentManager().popBackStackImmediate();
            transaction.commit();

        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count != 0) {
            startActivity(new Intent(this, MainActivity.class));
            finish();


        } else {
            super.onBackPressed();

        }
    }

}
