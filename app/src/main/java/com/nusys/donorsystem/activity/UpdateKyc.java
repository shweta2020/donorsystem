package com.nusys.donorsystem.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.SharedPreference_main;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE10;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE2;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE3;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE4;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE5;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE6;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE7;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE8;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE9;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE10;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE2;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE3;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE4;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE5;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE6;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE7;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE8;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE9;
import static com.nusys.donorsystem.commonModule.Constants.KYC;


/**
 * Created By: Shweta Agarwal
 * Created DAte:16-07-2020
 **/
public class UpdateKyc extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;
    LinearLayout llBackActivity;
    TextView tvToolbarHead;

    EditText etDocName1, etDocName2, etDocName3, etDocName4, etDocName5, etDocName6, etDocName7, etDocName8, etDocName9, etDocName10;
    ImageView ivDocFile1, ivDocFile2, ivDocFile3, ivDocFile4, ivDocFile5, ivDocFile6, ivDocFile7, ivDocFile8, ivDocFile9, ivDocFile10;
    TextView tvChooseFile1, tvChooseFile2, tvChooseFile3, tvChooseFile4, tvChooseFile5, tvChooseFile6, tvChooseFile7, tvChooseFile8, tvChooseFile9, tvChooseFile10;
    TextView tvAdd1, tvAdd2, tvAdd3, tvAdd4, tvAdd5, tvAdd6, tvAdd7, tvAdd8, tvAdd9, tvAdd10;
    TextView tvAddMore1, tvAddMore2, tvAddMore3, tvAddMore4, tvAddMore5, tvAddMore6, tvAddMore7, tvAddMore8, tvAddMore9;
    TextView tvDelete2, tvDelete3, tvDelete4, tvDelete5, tvDelete6, tvDelete7, tvDelete8, tvDelete9, tvDelete10;
    LinearLayout llBase2, llBase3, llBase4, llBase5, llBase6, llBase7, llBase8, llBase9, llBase10;
    private String userImageBase64, userImageBase64_2, userImageBase64_3, userImageBase64_4, userImageBase64_5, userImageBase64_6, userImageBase64_7, userImageBase64_8, userImageBase64_9, userImageBase64_10;
    private String fileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_kyc);
        initialization();
        action();
    }

    public void initialization() {
        context = UpdateKyc.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);

        etDocName1 = findViewById(R.id.et_doc_name1);
        ivDocFile1 = findViewById(R.id.iv_doc_file1);
        tvChooseFile1 = findViewById(R.id.tv_choose_file1);
        tvAdd1 = findViewById(R.id.tv_add1);
        tvAddMore1 = findViewById(R.id.tv_add_more1);

        etDocName2 = findViewById(R.id.et_doc_name2);
        ivDocFile2 = findViewById(R.id.iv_doc_file2);
        tvChooseFile2 = findViewById(R.id.tv_choose_file2);
        tvAdd2 = findViewById(R.id.tv_add2);
        tvAddMore2 = findViewById(R.id.tv_add_more2);
        tvDelete2 = findViewById(R.id.tv_delete2);
        llBase2 = findViewById(R.id.ll_base2);

        etDocName3 = findViewById(R.id.et_doc_name3);
        ivDocFile3 = findViewById(R.id.iv_doc_file3);
        tvChooseFile3 = findViewById(R.id.tv_choose_file3);
        tvAdd3 = findViewById(R.id.tv_add3);
        tvAddMore3 = findViewById(R.id.tv_add_more3);
        tvDelete3 = findViewById(R.id.tv_delete3);
        llBase3 = findViewById(R.id.ll_base3);

        etDocName4 = findViewById(R.id.et_doc_name4);
        ivDocFile4 = findViewById(R.id.iv_doc_file4);
        tvChooseFile4 = findViewById(R.id.tv_choose_file4);
        tvAdd4 = findViewById(R.id.tv_add4);
        tvAddMore4 = findViewById(R.id.tv_add_more4);
        tvDelete4 = findViewById(R.id.tv_delete4);
        llBase4 = findViewById(R.id.ll_base4);

        etDocName5 = findViewById(R.id.et_doc_name5);
        ivDocFile5 = findViewById(R.id.iv_doc_file5);
        tvChooseFile5 = findViewById(R.id.tv_choose_file5);
        tvAdd5 = findViewById(R.id.tv_add5);
        tvAddMore5 = findViewById(R.id.tv_add_more5);
        tvDelete5 = findViewById(R.id.tv_delete5);
        llBase5 = findViewById(R.id.ll_base5);

        etDocName6 = findViewById(R.id.et_doc_name6);
        ivDocFile6 = findViewById(R.id.iv_doc_file6);
        tvChooseFile6 = findViewById(R.id.tv_choose_file6);
        tvAdd6 = findViewById(R.id.tv_add6);
        tvAddMore6 = findViewById(R.id.tv_add_more6);
        tvDelete6 = findViewById(R.id.tv_delete6);
        llBase6 = findViewById(R.id.ll_base6);

        etDocName7 = findViewById(R.id.et_doc_name7);
        ivDocFile7 = findViewById(R.id.iv_doc_file7);
        tvChooseFile7 = findViewById(R.id.tv_choose_file7);
        tvAdd7 = findViewById(R.id.tv_add7);
        tvAddMore7 = findViewById(R.id.tv_add_more7);
        tvDelete7 = findViewById(R.id.tv_delete7);
        llBase7 = findViewById(R.id.ll_base7);

        etDocName8 = findViewById(R.id.et_doc_name8);
        ivDocFile8 = findViewById(R.id.iv_doc_file8);
        tvChooseFile8 = findViewById(R.id.tv_choose_file8);
        tvAdd8 = findViewById(R.id.tv_add8);
        tvAddMore8 = findViewById(R.id.tv_add_more8);
        tvDelete8 = findViewById(R.id.tv_delete8);
        llBase8 = findViewById(R.id.ll_base8);

        etDocName9 = findViewById(R.id.et_doc_name9);
        ivDocFile9 = findViewById(R.id.iv_doc_file9);
        tvChooseFile9 = findViewById(R.id.tv_choose_file9);
        tvAdd9 = findViewById(R.id.tv_add9);
        tvAddMore9 = findViewById(R.id.tv_add_more9);
        tvDelete9 = findViewById(R.id.tv_delete9);
        llBase9 = findViewById(R.id.ll_base9);

        etDocName10 = findViewById(R.id.et_doc_name10);
        ivDocFile10 = findViewById(R.id.iv_doc_file10);
        tvChooseFile10 = findViewById(R.id.tv_choose_file10);
        tvAdd10 = findViewById(R.id.tv_add10);
        tvDelete10 = findViewById(R.id.tv_delete10);
        llBase10 = findViewById(R.id.ll_base10);


    }

    public void action() {
        /* finish activity when click on bake arrow on toolbar
         * Created Date: 16-07-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /* set text on toolbar
         * Created Date: 16-07-2020
         */
        tvToolbarHead.setText("KYC Update");

        /* set visibility on add and delete buttons
         * Created Date: 16-07-2020
         */
        tvAdd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName1.getText().toString())) {

                    etDocName1.setError("field can't be empty");

                } else if (ivDocFile1.getDrawable() == null) {
                    tvChooseFile1.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName1.getText().toString(), userImageBase64);
                    llBase2.setVisibility(View.VISIBLE);
                    tvAdd1.setAlpha((float) 0.7);
                }


            }
        });

        tvAddMore1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase2.setVisibility(View.VISIBLE);
                tvAddMore1.setAlpha((float) 0.7);
            }
        });


        tvDelete2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase2.setVisibility(View.GONE);
                tvAddMore1.setVisibility(View.VISIBLE);
                tvAdd1.setVisibility(View.GONE);
                tvAddMore1.setAlpha(1);
            }
        });

        tvAdd2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName2.getText().toString())) {

                    etDocName2.setError("field can't be empty");

                } else if (ivDocFile2.getDrawable() == null) {
                    tvChooseFile2.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName2.getText().toString(), userImageBase64_2);
                    llBase3.setVisibility(View.VISIBLE);
                    tvDelete2.setVisibility(View.GONE);
                    tvAdd2.setAlpha((float) 0.7);
                }

            }
        });

        tvAddMore2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase3.setVisibility(View.VISIBLE);
                tvAddMore2.setAlpha((float) 0.7);
            }
        });

        tvDelete3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase3.setVisibility(View.GONE);
                tvAddMore2.setVisibility(View.VISIBLE);
                tvAdd2.setVisibility(View.GONE);
                tvAddMore2.setAlpha(1);
                //  tvDelete2.setVisibility(View.VISIBLE);
            }
        });

        tvAdd3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName3.getText().toString())) {

                    etDocName3.setError("field can't be empty");

                } else if (ivDocFile3.getDrawable() == null) {
                    tvChooseFile3.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName3.getText().toString(), userImageBase64_3);
                    llBase4.setVisibility(View.VISIBLE);
                    tvDelete3.setVisibility(View.GONE);
                    tvAdd3.setAlpha((float) 0.7);
                }


            }
        });

        tvAddMore3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase4.setVisibility(View.VISIBLE);
                tvAddMore3.setAlpha((float) 0.7);
            }
        });
        tvDelete4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase4.setVisibility(View.GONE);
                tvAddMore3.setVisibility(View.VISIBLE);
                tvAdd3.setVisibility(View.GONE);
                tvAddMore3.setAlpha(1);
                // tvDelete3.setVisibility(View.VISIBLE);
            }
        });
        tvAdd4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName4.getText().toString())) {

                    etDocName4.setError("field can't be empty");

                } else if (ivDocFile4.getDrawable() == null) {
                    tvChooseFile4.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName4.getText().toString(), userImageBase64_4);
                    llBase5.setVisibility(View.VISIBLE);
                    tvDelete4.setVisibility(View.GONE);
                    tvAdd4.setAlpha((float) 0.7);
                }

            }
        });
        tvAddMore4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase5.setVisibility(View.VISIBLE);
                tvAddMore4.setAlpha((float) 0.7);
            }
        });
        tvDelete5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase5.setVisibility(View.GONE);
                tvAddMore4.setVisibility(View.VISIBLE);
                tvAdd4.setVisibility(View.GONE);
                tvAddMore4.setAlpha(1);
                //  tvDelete4.setVisibility(View.VISIBLE);
            }
        });
        tvAdd5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName5.getText().toString())) {

                    etDocName5.setError("field can't be empty");

                } else if (ivDocFile5.getDrawable() == null) {
                    tvChooseFile5.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName5.getText().toString(), userImageBase64_5);
                    llBase6.setVisibility(View.VISIBLE);
                    tvDelete5.setVisibility(View.GONE);
                    tvAdd5.setAlpha((float) 0.7);
                }

            }
        });
        tvAddMore5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase6.setVisibility(View.VISIBLE);
                tvAddMore5.setAlpha((float) 0.7);
            }
        });
        tvDelete6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase6.setVisibility(View.GONE);
                tvAddMore5.setVisibility(View.VISIBLE);
                tvAdd5.setVisibility(View.GONE);
                tvAddMore5.setAlpha(1);
                //   tvDelete5.setVisibility(View.VISIBLE);
            }
        });
        tvAdd6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName6.getText().toString())) {

                    etDocName6.setError("field can't be empty");

                } else if (ivDocFile6.getDrawable() == null) {
                    tvChooseFile6.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName6.getText().toString(), userImageBase64_6);
                    llBase7.setVisibility(View.VISIBLE);
                    tvDelete6.setVisibility(View.GONE);
                    tvAdd6.setAlpha((float) 0.7);
                }

            }
        });
        tvAddMore6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase7.setVisibility(View.VISIBLE);
                tvAddMore6.setAlpha((float) 0.7);
            }
        });
        tvDelete7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase7.setVisibility(View.GONE);
                tvAddMore6.setVisibility(View.VISIBLE);
                tvAdd6.setVisibility(View.GONE);
                tvAddMore6.setAlpha(1);
                //    tvDelete6.setVisibility(View.VISIBLE);
            }
        });
        tvAdd7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName7.getText().toString())) {

                    etDocName7.setError("field can't be empty");

                } else if (ivDocFile7.getDrawable() == null) {
                    tvChooseFile7.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName7.getText().toString(), userImageBase64_7);
                    llBase8.setVisibility(View.VISIBLE);
                    tvDelete7.setVisibility(View.GONE);
                    tvAdd7.setAlpha((float) 0.7);
                }

            }
        });
        tvAddMore7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase8.setVisibility(View.VISIBLE);
                tvAddMore7.setAlpha((float) 0.7);
            }
        });
        tvDelete8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase8.setVisibility(View.GONE);
                tvAddMore7.setVisibility(View.VISIBLE);
                tvAdd7.setVisibility(View.GONE);
                tvAddMore7.setAlpha(1);
                //  tvDelete7.setVisibility(View.VISIBLE);
            }
        });
        tvAdd8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName8.getText().toString())) {

                    etDocName8.setError("field can't be empty");

                } else if (ivDocFile8.getDrawable() == null) {
                    tvChooseFile8.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName8.getText().toString(), userImageBase64_8);
                    llBase9.setVisibility(View.VISIBLE);
                    tvDelete8.setVisibility(View.GONE);
                    tvAdd8.setAlpha((float) 0.7);
                }

            }
        });
        tvAddMore8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase9.setVisibility(View.VISIBLE);
                tvAddMore8.setAlpha((float) 0.7);
            }
        });
        tvDelete9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase9.setVisibility(View.GONE);
                tvAddMore8.setVisibility(View.VISIBLE);
                tvAdd8.setVisibility(View.GONE);
                tvAddMore8.setAlpha(1);
                //   tvDelete8.setVisibility(View.VISIBLE);
            }
        });
        tvAdd9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName9.getText().toString())) {

                    etDocName9.setError("field can't be empty");

                } else if (ivDocFile9.getDrawable() == null) {
                    tvChooseFile9.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName9.getText().toString(), userImageBase64_9);
                    llBase10.setVisibility(View.VISIBLE);
                    tvDelete9.setVisibility(View.GONE);
                    tvAdd9.setAlpha((float) 0.7);
                }

            }
        });
        tvAddMore9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase10.setVisibility(View.VISIBLE);
                tvAddMore9.setAlpha((float) 0.7);
            }
        });
        tvDelete10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llBase10.setVisibility(View.GONE);
                tvAddMore9.setVisibility(View.VISIBLE);
                tvAdd9.setVisibility(View.GONE);
                tvAddMore9.setAlpha(1);
                //   tvDelete9.setVisibility(View.VISIBLE);
            }
        });
        tvAdd10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etDocName10.getText().toString())) {

                    etDocName10.setError("field can't be empty");

                } else if (ivDocFile10.getDrawable() == null) {
                    tvChooseFile10.setTag("field can't be empty");
                    // tvChooseFile1.setError("field can't be empty");
                } else {
                    update_kyc(etDocName10.getText().toString(), userImageBase64_10);

                    tvAdd10.setAlpha((float) 0.7);
                }

            }
        });
        /*  action when click on image
         *  Created Date: 13-07-2020
         */
        tvChooseFile1.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);

            }
        });

        tvChooseFile2.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE2, GALLERY_REQUEST_CODE2);

            }
        });

        tvChooseFile3.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE3, GALLERY_REQUEST_CODE3);

            }
        });

        tvChooseFile4.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE4, GALLERY_REQUEST_CODE4);

            }
        });

        tvChooseFile5.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE5, GALLERY_REQUEST_CODE5);

            }
        });

        tvChooseFile6.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE6, GALLERY_REQUEST_CODE6);

            }
        });

        tvChooseFile7.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE7, GALLERY_REQUEST_CODE7);

            }
        });

        tvChooseFile8.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE8, GALLERY_REQUEST_CODE8);

            }
        });

        tvChooseFile9.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE9, GALLERY_REQUEST_CODE9);

            }
        });

        tvChooseFile10.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE10, GALLERY_REQUEST_CODE10);

            }
        });


    }

    /*  method for open gallery and camera for uploading image- below all 6 methods
     *  Created Date: 03-07-2020
     */

    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile1.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64);


                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile1.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;


            case CAMERA_REQUEST_CODE2:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile2.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64_2 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64_2);


                }
                break;
            case GALLERY_REQUEST_CODE2:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile2.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64_2 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64_2);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;

            case CAMERA_REQUEST_CODE3:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile3.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64_3 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64_3);


                }
                break;
            case GALLERY_REQUEST_CODE3:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile3.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64_3 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64_3);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;

            case CAMERA_REQUEST_CODE4:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile4.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64_4 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64_4);


                }
                break;
            case GALLERY_REQUEST_CODE4:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile4.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64_4 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64_4);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;

            case CAMERA_REQUEST_CODE5:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile5.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64_5 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64_5);


                }
                break;
            case GALLERY_REQUEST_CODE5:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile5.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64_5 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64_5);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;

            case CAMERA_REQUEST_CODE6:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile6.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64_6 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64_6);


                }
                break;
            case GALLERY_REQUEST_CODE6:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile6.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64_6 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64_6);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;

            case CAMERA_REQUEST_CODE7:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile7.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64_7 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64_7);


                }
                break;
            case GALLERY_REQUEST_CODE7:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile7.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64_7 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64_7);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;

            case CAMERA_REQUEST_CODE8:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile8.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64_8 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64_8);


                }
                break;
            case GALLERY_REQUEST_CODE8:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile8.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64_8 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64_8);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;

            case CAMERA_REQUEST_CODE9:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile9.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64_9 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64_9);


                }
                break;
            case GALLERY_REQUEST_CODE9:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile9.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64_9 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64_9);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;

            case CAMERA_REQUEST_CODE10:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivDocFile10.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64_10 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("exam", userImageBase64_10);


                }
                break;
            case GALLERY_REQUEST_CODE10:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivDocFile10.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64_10 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64_10);
                        // post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());

                break;
        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView camera = dialog.findViewById(R.id.camera);
        TextView gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }
    /*  method for kyc
     *  Created Date: 17-07-2020
     * Updated Date:
     */

    private void update_kyc(String doc, String image) {

        //  pd.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("doc_name", doc);
            jsonBody.put("image", image);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + KYC, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Integer status = response.getInt("status");
                            if (status == 200) {
                                //  pd.dismiss();
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();

                            } else {
                                //   pd.dismiss();
                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            //pd.dismiss();

                            e.printStackTrace();
                        }
                        // Toast.makeText(SignUpStepTwo.this, "" + response.toString(), Toast.LENGTH_LONG).show();
                        Log.e("response1", response.toString());
                        //resultTextView.setText("String Response : "+ response.toString());
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //    pd.dismiss();

                //  resultTextView.setText("Error getting response");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }


}