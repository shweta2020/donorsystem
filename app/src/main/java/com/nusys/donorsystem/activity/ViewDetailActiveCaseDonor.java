package com.nusys.donorsystem.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.CaseByIDModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:21-07-2020
 * Updated Date:22-07-2020(add error layout and swipe refresh, progress bar)
 **/
public class ViewDetailActiveCaseDonor extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context context;
    SharedPreference_main sharedPreference_main;

    SwipeRefreshLayout refreshLayout;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    private ProgressDialog pd;
    FrameLayout flMain;

    LinearLayout llBackActivity;
    TextView tvToolbarHead;

    TextView tvHeading, tvAmount, tvDetail;
    ImageView ivImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_detail_active_case_donor);
        initialization();
        action();
    }

    public void initialization() {
        context = ViewDetailActiveCaseDonor.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        refreshLayout = findViewById(R.id.refresh_layout);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);
        flMain = findViewById(R.id.fl_main);

        tvHeading = findViewById(R.id.tv_heading);
        tvAmount = findViewById(R.id.tv_amount);
        tvDetail = findViewById(R.id.tv_detail);
        ivImg = findViewById(R.id.iv_img);

    }

    public void action() {
        /* finish activity when click on bake arrow on toolbar
         * Created Date: 21-07-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /* set text on toolbar
         * Created Date: 21-07-2020
         */
        tvToolbarHead.setText("Detail");
        /* calling method for view detail case
         * Created Date: 21-07-2020
         */
        view_case();
    }

    /*  view edit case value
     *  Created Date: 21-07-2020
     * Updated Date: 22-07-2020(add error view when network not connected)
     */
    private void view_case() {
        if (NetworkUtil.isConnected(context)) {
            pd.show();

            HashMap<String, String> map = new HashMap<>();

            map.put("case_id", getIntent().getStringExtra("caseId"));


            //   if (NetworkUtils.isConnected(getActivity())) {
            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<CaseByIDModel> call = serviceInterface.fetch_case_byId(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<CaseByIDModel>() {
                @Override
                public void onResponse(Call<CaseByIDModel> call, retrofit2.Response<CaseByIDModel> response) {

                    if (response.isSuccessful()) {

                        CaseByIDModel bean = response.body();
                        if (bean.getStatus() == 200) {

                            tvHeading.setText(bean.getData().get(0).getCaseTitle());
                            tvDetail.setText(bean.getData().get(0).getCaseDisc());
                            tvAmount.setText("R "+bean.getData().get(0).getRequiredMoney());

                            Glide.with(getApplicationContext())
                                    .load(bean.getData().get(0).getImage())
                                    .placeholder(R.drawable.user_icon)
                                    .into(ivImg);

                            pd.dismiss();

                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            flMain.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        flMain.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CaseByIDModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    flMain.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                view_case();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);
    }
}