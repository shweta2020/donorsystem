package com.nusys.donorsystem.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.adapter.AdapterActiveCaseR;
import com.nusys.donorsystem.adapter.AdapterBlog;
import com.nusys.donorsystem.adapter.AdapterChatComment;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.ActiveCaseRModel;
import com.nusys.donorsystem.model.BlogModel;
import com.nusys.donorsystem.model.GetChatModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.donorsystem.commonModule.Constants.ADD_CHAT;
import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.GET_CHAT;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:17-07-2020
 * Updated Date: 20-07-2020 (add methods and functionality)
 **/
public class Chat extends AppCompatActivity {


    SharedPreference_main sharedPreference_main;
    Context context;
    RecyclerView rvComment;
    LinearLayout llNoComment;
    EditText etComment;
    ImageView ivOpenGalley, ivSend;
    AdapterChatComment myAdapterComment;
    private String userImageBase64 = "";
    String etChatComment = "";
    GetChatModel bean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        initialization();
        action();
    }

    public void initialization() {
        sharedPreference_main = SharedPreference_main.getInstance(this);
        context = Chat.this;

        rvComment = findViewById(R.id.rv_comment);
        llNoComment = findViewById(R.id.ll_noComment);
        etComment = findViewById(R.id.et_comment);
        ivOpenGalley = findViewById(R.id.iv_open_galley);
        ivSend = findViewById(R.id.iv_send);


    }

    public void action() {
        /*method calling for fetching comments or chat
         * Created Date: 20-07-2020
         */
        getComments();

        /*method for open camera
         * Created Date: 20-07-2020
         */
        ivOpenGalley.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);
            }
        });
        /*send chat when click on ivsend
         * Created Date: 20-07-2020
         */
        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!userImageBase64.equals("")) {
                    DoComment(etChatComment);
                } else {
                    if (TextUtils.isEmpty(etComment.getText().toString())) {
                        etComment.setError("field can't be empty");
                    } else {

                        DoComment(etComment.getText().toString());

                    }
                }
            }
        });
    }

    /*method for get chat
     * Created Date: 20-07-2020
     */

    private void getComments() {

        HashMap<String, String> map = new HashMap<>();

        map.put("user_id", sharedPreference_main.getUserId());

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<GetChatModel> call = serviceInterface.get_chat(sharedPreference_main.getToken(), CONTENT_TYPE, map);
        call.enqueue(new Callback<GetChatModel>() {


            @Override
            public void onResponse(Call<GetChatModel> call, retrofit2.Response<GetChatModel> response) {

                if (response.isSuccessful()) {

                    bean = response.body();

                    if (bean.getStatus() == 200) {
                        GetChatModel view = response.body();


                        myAdapterComment = new AdapterChatComment(context, bean.getData());
                        rvComment.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                        rvComment.setItemAnimator(new DefaultItemAnimator());
                        rvComment.setAdapter(myAdapterComment);
                        Log.e("Group_response", view.toString());

                    } else {

                        Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetChatModel> call, Throwable t) {

                Log.e("error", t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    /*method for do chat
     * Created Date: 20-07-2020
     */
    private void DoComment(final String etChatComment) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        final JSONObject jsonBody = new JSONObject();
        try {


            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("msg", etChatComment);
            jsonBody.put("image", userImageBase64);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ADD_CHAT, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Integer status = response.getInt("status");

                            if (status == 200) {
                                etComment.setText("");
                                userImageBase64 = "";
                                ivOpenGalley.setImageResource(R.drawable.ic_photo_camera_black_24dp);
                                getComments();


                            } else {
                                Toast.makeText(context, "error" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    /*method for open gallery and camera
     * Created Date: 20-07-2020
     */
    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {


                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivOpenGalley.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);

                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivOpenGalley.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);


                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
//                    tv_userImage.setText(imageUri.getPath());
                break;


        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        //  printLog(imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.chooser);
        TextView camera = dialog.findViewById(R.id.camera);
        TextView gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }


}