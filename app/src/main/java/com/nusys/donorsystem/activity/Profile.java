package com.nusys.donorsystem.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.CityModel;
import com.nusys.donorsystem.model.CountryModel;
import com.nusys.donorsystem.model.ProfileModel;
import com.nusys.donorsystem.model.StateModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.PROFILE_PIC_UPDATE;
import static com.nusys.donorsystem.commonModule.Constants.UPDATE_PROFILE;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:01-07-2020
 * Updated Date:22-07-2020(add error layout and progress bar)
 **/
public class Profile extends AppCompatActivity {
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    private ProgressDialog pd;
    LinearLayout llMain;

    Context context;
    SharedPreference_main sharedPreference_main;
    LinearLayout llBackActivity;
    TextView tvToolbarHead;

    EditText etFirstName, etMiddleName, etMobileNum, etEmailId, etAddress1, etAddress2, etUserType, etState, etCity;
    TextView tvUpdate;
    ImageView imgProfile, imgPickFrom;
    Spinner spCountry, spState, spCity;
    LinearLayout llState, llCity;

    String spCountryID, spCityID, spStateID, spUserTypeID;

    private ArrayList<String> spinCountry = new ArrayList<String>();
    private ArrayList<String> spinCountry_id = new ArrayList<String>();

    private ArrayList<String> spinState = new ArrayList<String>();
    private ArrayList<String> spinState_id = new ArrayList<String>();

    private ArrayList<String> spinCity = new ArrayList<String>();
    private ArrayList<String> spinCity_id = new ArrayList<String>();

    private ArrayList<String> spinUserType = new ArrayList<String>();
    private ArrayList<String> spUserType_id = new ArrayList<String>();

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String country_fetch = "", city_fetch, state_fetch, usertype_fetch;
    private String userImageBase64;
    private String fileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);
        initialization();
        action();
    }

    public void initialization() {
        context = Profile.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);


        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        llMain = findViewById(R.id.ll_main);

        etFirstName = findViewById(R.id.et_first_name);
        etMiddleName = findViewById(R.id.et_middle_name);
        etMobileNum = findViewById(R.id.et_mobile_num);
        etEmailId = findViewById(R.id.et_email_id);
        etAddress1 = findViewById(R.id.et_address1);
        etAddress2 = findViewById(R.id.et_address2);
        spCountry = findViewById(R.id.sp_country);
        spState = findViewById(R.id.sp_state);
        spCity = findViewById(R.id.sp_city);
        etUserType = findViewById(R.id.et_usertype);
        tvUpdate = findViewById(R.id.tv_update);
        imgProfile = findViewById(R.id.img_Profile);
        imgPickFrom = findViewById(R.id.img_pickFrom);
        etState = findViewById(R.id.et_state);
        llState = findViewById(R.id.ll_state);
        etCity = findViewById(R.id.et_city);
        llCity = findViewById(R.id.ll_city);

        spinCountry.add("Select Country");
        spinCountry_id.add("-1");

        spinUserType.add("Select User Type");
        spUserType_id.add("-1");

        spCountry.setOnTouchListener(spinnerOnTouch);
        spState.setOnTouchListener(spinnerOnTouchState);
    }

    public void action() {
        /* finish activity when click on bake arrow
         * Created Date: 02-07-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /*  method for viewing user profile
         *  Created Date: 03-07-2020
         */
        user_profile();
        /*  action for taking spinner country value
         *  Created Date: 03-07-2020
         * Updated Date:
         */

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spCountryID = spinCountry_id.get(position);
                view_state_list(spinCountry_id.get(position));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /*  action for taking spinner State value
         *  Created Date: 03-07-2020
         * Updated Date:
         */
        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, ""+parent.getSelectedItemId(), Toast.LENGTH_SHORT).show();
                spStateID = spinState_id.get(position);
                //   Toast.makeText(context, "Subjectclick"+id_sub1, Toast.LENGTH_SHORT).show();

                view_city_list(spStateID);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*  action for taking spinner city value
         *  Created Date: 03-07-2020
         * Updated Date:
         */
        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(context, ""+parent.getSelectedItemId(), Toast.LENGTH_SHORT).show();

                spCityID = spinCity_id.get(position);
                //  Toast.makeText(context, "unit"+id_unit, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*  method for country list
         *  Created Date: 03-07-2020
         */
        view_country_list();

        /*  action when click on image
         *  Created Date: 03-07-2020
         */
        imgPickFrom.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);

            }
        });
        /*  action when click on update button
         *  Created Date: 03-07-2020
         * Updated Date:22-07-2020(add spinner validation)
         */
        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((TextUtils.isEmpty(etFirstName.getText().toString()))) {

                    etFirstName.setError("field can't be empty");

                } else if ((TextUtils.isEmpty(etMiddleName.getText().toString()))) {

                    etMiddleName.setError("field can't be empty");

                } else if (TextUtils.isEmpty(etAddress1.getText().toString())) {
                    etAddress1.setError("field can't be empty");

                } else if (TextUtils.isEmpty(etAddress2.getText().toString())) {
                    etAddress2.setError("field can't be empty");

                } else if (spCountryID.equals("-1")) {

                    Toast.makeText(context, "Please Select Country", Toast.LENGTH_SHORT).show();

                } else if (spStateID.equals("-1")) {

                    Toast.makeText(context, "Please Select State", Toast.LENGTH_SHORT).show();

                } else if (spCityID.equals("-1")) {

                    Toast.makeText(context, "Please Select City", Toast.LENGTH_SHORT).show();

                }else {
                    update_profile();
                }
            }
        });


    }

    /*  action when click on country spinner it gives visibility of state spinner
     *  Created Date: 04-07-2020
     */
    private View.OnTouchListener spinnerOnTouch = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                llState.setVisibility(View.VISIBLE);
                etState.setVisibility(View.GONE);
            }
            return false;
        }
    };

    /*  action when click on state spinner it gives visibility of city spinner
     *  Created Date: 04-07-2020
     */
    private View.OnTouchListener spinnerOnTouchState = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                llCity.setVisibility(View.VISIBLE);
                etCity.setVisibility(View.GONE);
            }
            return false;
        }
    };

    /*  view user profile values
     *  Created Date: 03-07-2020
     * Updated Date:03-07-2020(fetch value in  country, state and city)
     * Updated Date:04-07-2020(set text on et_state and et_city)
     * Updated Date: 22-07-2020(add error view when network not connected)
     */
    private void user_profile() {
        if (NetworkUtil.isConnected(context)) {

            pd.show();
            HashMap<String, String> map = new HashMap<>();

            map.put("user_id", sharedPreference_main.getUserId());

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<ProfileModel> call = serviceInterface.profile(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<ProfileModel>() {
                @Override
                public void onResponse(Call<ProfileModel> call, retrofit2.Response<ProfileModel> response) {

                    if (response.isSuccessful()) {

                        ProfileModel bean = response.body();
                        if (bean.getStatus() == 200) {

                            country_fetch = bean.getData().get(0).getCountry();
                            etState.setText(bean.getData().get(0).getState());
                            spStateID = bean.getData().get(0).getStateId();
                            etCity.setText(bean.getData().get(0).getCity());
                            spCityID = bean.getData().get(0).getCityId();
                            etUserType.setText(bean.getData().get(0).getUserType());
                            etMobileNum.setText(bean.getData().get(0).getPhoneNo());
                            etFirstName.setText(bean.getData().get(0).getFirstname());
                            etMiddleName.setText(bean.getData().get(0).getMiddlename());
                            etEmailId.setText(bean.getData().get(0).getEmail());
                            etAddress1.setText(bean.getData().get(0).getAddress1());
                            etAddress2.setText(bean.getData().get(0).getAddress2());
                            Glide.with(getApplicationContext())
                                    .load(bean.getData().get(0).getProfilePic())
                                    .placeholder(R.drawable.user_icon)
                                    .into(imgProfile);
                            pd.dismiss();

                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            llMain.setVisibility(View.GONE);
                            imgPickFrom.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        llMain.setVisibility(View.GONE);
                        imgPickFrom.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ProfileModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    llMain.setVisibility(View.GONE);
                    imgPickFrom.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }
    /*  method for fetching country list
     *  Created Date: 03-07-2020
     */

    private void view_country_list() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<CountryModel> call = serviceInterface.country_list(CONTENT_TYPE);
        call.enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(Call<CountryModel> call, retrofit2.Response<CountryModel> response) {
                if (response.isSuccessful()) {
                    CountryModel bean = response.body();
                    if (bean.getStatus() == 200) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String countryName = bean.getData().get(i).getCountryName();
                            String countryID = bean.getData().get(i).getCountryId();


                            spinCountry.add(countryName);
                            spinCountry_id.add(countryID);
                        }

                    } else {
                        Toast.makeText(context, "something is wrong", Toast.LENGTH_SHORT).show();
                    }
                    spCountry.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCountry));
                    spCountryFetch();
                } else {
                    Toast.makeText(context, "Something is wrong when fetching country ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountryModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();

            }
        });


    }
    /*  method for set country value in country spinner
     *  Created Date: 03-07-2020
     */

    private void spCountryFetch() {
        String myString2 = country_fetch;//the value you want the position for
        ArrayAdapter myAdap2 = (ArrayAdapter) spCountry.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myAdap2.getPosition(myString2);
        spCountry.setSelection(spinnerPosition);
    }

    /*  method for fetching state list
     *  Created Date: 03-07-2020
     */

    private void view_state_list(String id_country) {
        spinState.clear();
        spinState_id.clear();
        spinState.add("Select State");
        spinState_id.add("-1");
        HashMap<String, String> map = new HashMap<>();
        map.put("country_id", id_country);

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<StateModel> call = serviceInterface.state_list(map);
        call.enqueue(new Callback<StateModel>() {
            @Override
            public void onResponse(Call<StateModel> call, Response<StateModel> response) {
                if (response.isSuccessful()) {
                    StateModel bean = response.body();
                    if (bean.getStatus().equals(200)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String stateName = bean.getData().get(i).getStateName();
                            String stateId = bean.getData().get(i).getStateId();


                            spinState.add(stateName);
                            spinState_id.add(stateId);
                        }

                        spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

                    } else {
                        spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

                    }

                } else {
                    spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

                }
            }


            @Override
            public void onFailure(Call<StateModel> call, Throwable t) {
                spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));


            }
        });


    }

    /*  method for fetching city list
     *  Created Date: 03-07-2020
     */

    private void view_city_list(String id_state) {
        spinCity.clear();
        spinCity_id.clear();
        spinCity.add("Select City");
        spinCity_id.add("-1");
        HashMap<String, String> map = new HashMap<>();
        map.put("state_id", id_state);

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<CityModel> call = serviceInterface.city_list(map);
        call.enqueue(new Callback<CityModel>() {
            @Override
            public void onResponse(Call<CityModel> call, Response<CityModel> response) {
                if (response.isSuccessful()) {
                    CityModel bean = response.body();
                    if (bean.getStatus().equals(200)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String cityName = bean.getData().get(i).getCityName();
                            String cityId = bean.getData().get(i).getCityId();

                            spinCity.add(cityName);
                            spinCity_id.add(cityId);
                        }
                        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                    } else {
                        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));
                    }

                } else {
                    spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                }
            }

            @Override
            public void onFailure(Call<CityModel> call, Throwable t) {
                spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));


            }
        });


    }

    /*  method for update profile
     *  Created Date: 03-07-2020
     * Updated Date:04-07-2020(remove fields which is not editable like email,phone and usertype)
     */

    private void update_profile() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("firstname", etFirstName.getText().toString());
            jsonBody.put("middlename", etMiddleName.getText().toString());
            jsonBody.put("address1", etAddress1.getText().toString());
            jsonBody.put("address2", etAddress2.getText().toString());
            jsonBody.put("city_id", spCityID);
            jsonBody.put("state_id", spStateID);
            jsonBody.put("country_id", spCountryID);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + UPDATE_PROFILE, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Integer status = response.getInt("status");
                            if (status == 200) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();

                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    /*  method for open gallery and camera for uploading image- below all 6 methods
     *  Created Date: 03-07-2020
     */

    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    imgProfile.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("excam", userImageBase64);
                    post_Json_Request_Image();

                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    imgProfile.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);
                        post_Json_Request_Image();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                break;

        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView camera = dialog.findViewById(R.id.camera);
        TextView gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();

            }
        });
    }

    /*  method for update image
     *  Created Date: 03-07-2020
     * Updated Date:07-07-2020
     */

    private void post_Json_Request_Image() {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("profile_pic", userImageBase64);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + PROFILE_PIC_UPDATE, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            Boolean status = response.getBoolean("status");
                            if (status) {
                                //sharedPreference_main.setUserImage(response.getString("image"));
                                JSONObject jsonObject2 = response.getJSONObject("data");
                                // sharedPreference_main.setUserImage(jsonObject2.getString("image"));
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);

    }

}
