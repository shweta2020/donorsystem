package com.nusys.donorsystem.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.CaseByIDModel;
import com.nusys.donorsystem.model.CaseTypeModel;
import com.nusys.donorsystem.model.CityModel;
import com.nusys.donorsystem.model.CountryModel;
import com.nusys.donorsystem.model.StateModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.CAMERA_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.CONTENT_TYPE;
import static com.nusys.donorsystem.commonModule.Constants.GALLERY_REQUEST_CODE;
import static com.nusys.donorsystem.commonModule.Constants.UPDATE_CASE;
import static com.nusys.donorsystem.commonModule.Extension.openCalender;
import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:13-07-2020
 * Updated Date:21-07-2020(add error layout and progress bar)
 **/
public class EditAddCase extends AppCompatActivity {

    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;
    private ProgressDialog pd;
    LinearLayout llMain;

    Context context;
    SharedPreference_main sharedPreference_main;
    LinearLayout llBackActivity;
    TextView tvToolbarHead;

    EditText etTitle, etDetail, etReqAmount, etState, etCity;
    TextView tvDateFrom, tvDateTo;
    ImageView ivChooseFile;
    Button btnChoosefile, btnUpdate;
    Spinner spCountry, spState, spCity, spCaseCat;

    String spCountryID, spCityID, spStateID, spCaseCatID;

    private ArrayList<String> spinCountry = new ArrayList<String>();
    private ArrayList<String> spinCountry_id = new ArrayList<String>();

    private ArrayList<String> spinState = new ArrayList<String>();
    private ArrayList<String> spinState_id = new ArrayList<String>();

    private ArrayList<String> spinCity = new ArrayList<String>();
    private ArrayList<String> spinCity_id = new ArrayList<String>();

    private ArrayList<String> spinCaseCat = new ArrayList<String>();
    private ArrayList<String> spinCaseCat_id = new ArrayList<String>();

    String country_fetch, cat_fetch = "";
    LinearLayout llState, llCity;
    private int mYear, mMonth, mDay;
    private String userImageBase64 = "";
    private String fileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_add_case);
        initialization();
        action();
    }

    public void initialization() {

        context = EditAddCase.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);
        llBackActivity = findViewById(R.id.ll_back_activity);
        tvToolbarHead = findViewById(R.id.tv_toolbar_head);

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        llMain = findViewById(R.id.ll_main);

        etTitle = findViewById(R.id.et_title);
        etDetail = findViewById(R.id.et_detail);
        etReqAmount = findViewById(R.id.et_req_amount);
        tvDateFrom = findViewById(R.id.tv_date_from);
        tvDateTo = findViewById(R.id.tv_date_to);

        ivChooseFile = findViewById(R.id.iv_choose_file);
        btnChoosefile = findViewById(R.id.btn_choose_file);
        btnUpdate = findViewById(R.id.btn_update);
        spCountry = findViewById(R.id.sp_country);
        spState = findViewById(R.id.sp_state);
        spCity = findViewById(R.id.sp_city);
        spCaseCat = findViewById(R.id.sp_case_cat);
        etState = findViewById(R.id.et_state);
        llState = findViewById(R.id.ll_state);
        etCity = findViewById(R.id.et_city);
        llCity = findViewById(R.id.ll_city);


        spinCountry.add("Select Country");
        spinCountry_id.add("-1");

        spinCaseCat.add("Select Category");
        spinCaseCat_id.add("-1");

        spCountry.setOnTouchListener(spinnerOnTouch);
        spState.setOnTouchListener(spinnerOnTouchState);
    }

    public void action() {
        /* finish activity when click on bake arrow on toolbar
         * Created Date: 13-07-2020
         */

        llBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        /* set text on toolbar
         * Created Date: 13-07-2020
         */
        tvToolbarHead.setText("Edit Case");

        /*  action when click on image
         *  Created Date: 13-07-2020
         */
        btnChoosefile.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                openChooser(CAMERA_REQUEST_CODE, GALLERY_REQUEST_CODE);

            }
        });
        /* calling date picker when click on textbox
         *  Created Date: 13-07-2020
         */

        tvDateFrom.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                openCalender(context, mYear, mMonth, mDay, tvDateFrom);

            }

        });
        /* calling date picker when click on textbox
         *  Created Date: 13-07-2020
         */
        tvDateTo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                openCalender(context, mYear, mMonth, mDay, tvDateTo);

            }

        });
        /*  method for viewing case
         *  Created Date: 13-07-2020
         */
        view_case();
        /*  action for taking spinner country value
         *  Created Date: 13-07-2020
         * Updated Date:
         */

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spCountryID = spinCountry_id.get(position);
                view_state_list(spinCountry_id.get(position));


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*  action for taking spinner State value
         *  Created Date: 13-07-2020
         * Updated Date:
         */
        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spStateID = spinState_id.get(position);
                view_city_list(spStateID);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*  action for taking spinner city value
         *  Created Date: 13-07-2020
         * Updated Date:
         */
        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spCityID = spinCity_id.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*  method for country list
         *  Created Date: 01-07-2020
         */
        view_country_list();

        /*  method for case category list
         *  Created Date: 13-07-2020
         */
        view_case_cat();
        /*  action for taking spinner case category list
         *  Created Date: 13-07-2020
         * Updated Date:
         */

        spCaseCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spCaseCatID = spinCaseCat_id.get(position);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /* calling add case method when click on submit button
         *  Created Date: 13-07-2020
         * Updated Date:22-07-2020(add spinner validation and image validation)
         */
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spCaseCatID.equals("-1")) {

                    Toast.makeText(context, "Please Select Case Category", Toast.LENGTH_SHORT).show();

                } else if ((TextUtils.isEmpty(etTitle.getText().toString()))) {

                    etTitle.setError("field can't be empty");

                } else if ((TextUtils.isEmpty(etDetail.getText().toString()))) {

                    etDetail.setError("field can't be empty");

                } else if (TextUtils.isEmpty(etReqAmount.getText().toString())) {
                    etReqAmount.setError("field can't be empty");

                } else if (spCountryID.equals("-1")) {

                    Toast.makeText(context, "Please Select Country", Toast.LENGTH_SHORT).show();

                } else if (spStateID.equals("-1")) {

                    Toast.makeText(context, "Please Select State", Toast.LENGTH_SHORT).show();

                } else if (spCityID.equals("-1")) {

                    Toast.makeText(context, "Please Select City", Toast.LENGTH_SHORT).show();

                } else if (ivChooseFile.getDrawable() == null){
                    Toast.makeText(context, "Select Document", Toast.LENGTH_SHORT).show();
                } else {
                    update_case();
                }
            }
        });


    }

    /*  action when click on country spinner it gives visibility of state spinner
     *  Created Date: 13-07-2020
     */
    private View.OnTouchListener spinnerOnTouch = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                llState.setVisibility(View.VISIBLE);
                etState.setVisibility(View.GONE);
            }
            return false;
        }
    };

    /*  action when click on state spinner it gives visibility of city spinner
     *  Created Date: 13-07-2020
     */
    private View.OnTouchListener spinnerOnTouchState = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                llCity.setVisibility(View.VISIBLE);
                etCity.setVisibility(View.GONE);
            }
            return false;
        }
    };

    /*  view edit case value
     *  Created Date: 03-07-2020
     *  Updated Date:15-07-2020(fetch category and city)
     *  Updated Date: 21-07-2020(add error view when network not connected)
     */
    private void view_case() {
        if (NetworkUtil.isConnected(context)) {

            pd.show();
            HashMap<String, String> map = new HashMap<>();

            map.put("case_id", getIntent().getStringExtra("caseId"));

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<CaseByIDModel> call = serviceInterface.fetch_case_byId(sharedPreference_main.getToken(), CONTENT_TYPE, map);
            call.enqueue(new Callback<CaseByIDModel>() {
                @Override
                public void onResponse(Call<CaseByIDModel> call, retrofit2.Response<CaseByIDModel> response) {

                    if (response.isSuccessful()) {

                        CaseByIDModel bean = response.body();
                        if (bean.getStatus() == 200) {

                        /*country_fetch=bean.getData().get(0).getCountry();
                        etState.setText(bean.getData().get(0).getState());
                        spStateID=bean.getData().get(0).getStateId();
                       */
                            etCity.setText(bean.getData().get(0).getCityName());
                            spCityID = bean.getData().get(0).getCityId();
                            cat_fetch = bean.getData().get(0).getCatName();

                            etTitle.setText(bean.getData().get(0).getCaseTitle());
                            etDetail.setText(bean.getData().get(0).getCaseDisc());
                            etReqAmount.setText(bean.getData().get(0).getRequiredMoney());
                            tvDateFrom.setText(bean.getData().get(0).getDateFrom());
                            tvDateTo.setText(bean.getData().get(0).getDateTo());
                            userImageBase64 = convertUrlToBase64(bean.getData().get(0).getImage());

                            Glide.with(getApplicationContext())
                                    .load(bean.getData().get(0).getImage())
                                    .placeholder(R.drawable.user_icon)
                                    .into(ivChooseFile);
                            pd.dismiss();


                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            llMain.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        llMain.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CaseByIDModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    llMain.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    /**
     * @param url - web url
     * @return - Base64 String
     * Method used to Convert URL to Base64 String
     * Created Date: 15-07-2020
     */
    public String convertUrlToBase64(String url) {
        URL newurl;
        Bitmap bitmap;
        String base64 = "";
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            newurl = new URL(url);
            bitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            base64 = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64;
    }

    /*  method for fetching country list
     *  Created Date: 13-07-2020
     */

    private void view_country_list() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<CountryModel> call = serviceInterface.country_list(CONTENT_TYPE);
        call.enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(Call<CountryModel> call, retrofit2.Response<CountryModel> response) {
                if (response.isSuccessful()) {
                    CountryModel bean = response.body();
                    if (bean.getStatus() == 200) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String countryName = bean.getData().get(i).getCountryName();
                            String countryID = bean.getData().get(i).getCountryId();


                            spinCountry.add(countryName);
                            spinCountry_id.add(countryID);

                        }

                    } else {
                        Toast.makeText(context, "something is wrong", Toast.LENGTH_SHORT).show();
                    }
                    spCountry.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCountry));
                    spCountryFetch();
                } else {
                    Toast.makeText(context, "Something is wrong when fetching country ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CountryModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrong please try again later ", Toast.LENGTH_SHORT).show();

            }
        });


    }
    /*  method for set country value in country spinner
     *  Created Date: 13-07-2020
     */

    private void spCountryFetch() {
        String myString2 = country_fetch;//the value you want the position for
        ArrayAdapter myAdap2 = (ArrayAdapter) spCountry.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myAdap2.getPosition(myString2);
        spCountry.setSelection(spinnerPosition);
    }

    /*  method for fetching state list
     *  Created Date: 13-07-2020
     */

    private void view_state_list(String id_country) {
        spinState.clear();
        spinState_id.clear();
        spinState.add("Select State");
        spinState_id.add("-1");
        HashMap<String, String> map = new HashMap<>();
        map.put("country_id", id_country);

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<StateModel> call = serviceInterface.state_list(map);
        call.enqueue(new Callback<StateModel>() {
            @Override
            public void onResponse(Call<StateModel> call, Response<StateModel> response) {
                if (response.isSuccessful()) {
                    StateModel bean = response.body();
                    if (bean.getStatus().equals(200)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String stateName = bean.getData().get(i).getStateName();
                            String stateId = bean.getData().get(i).getStateId();


                            spinState.add(stateName);
                            spinState_id.add(stateId);
                        }
                        spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

                    } else {
                        spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));


                    }

                } else {
                    spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

                }
            }


            @Override
            public void onFailure(Call<StateModel> call, Throwable t) {
                spState.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinState));

            }
        });

    }

    /*  method for fetching city list
     *  Created Date: 13-07-2020
     */

    private void view_city_list(String id_state) {
        spinCity.clear();
        spinCity_id.clear();
        spinCity.add("Select City");
        spinCity_id.add("-1");
        HashMap<String, String> map = new HashMap<>();
        map.put("state_id", id_state);

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<CityModel> call = serviceInterface.city_list(map);
        call.enqueue(new Callback<CityModel>() {
            @Override
            public void onResponse(Call<CityModel> call, Response<CityModel> response) {
                if (response.isSuccessful()) {
                    CityModel bean = response.body();
                    if (bean.getStatus().equals(200)) {

                        for (int i = 0; i < bean.getData().size(); i++) {

                            String cityName = bean.getData().get(i).getCityName();
                            String cityId = bean.getData().get(i).getCityId();


                            spinCity.add(cityName);
                            spinCity_id.add(cityId);

                        }
                        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                    } else {
                        spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                    }

                } else {
                    spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

                }
            }


            @Override
            public void onFailure(Call<CityModel> call, Throwable t) {
                spCity.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCity));

            }
        });


    }

    /*  method for fetching case category list
     *  Created Date: 13-07-2020
     */
    private void view_case_cat() {

        ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
        Call<CaseTypeModel> call = serviceInterface.case_category(sharedPreference_main.getToken(), CONTENT_TYPE);
        call.enqueue(new Callback<CaseTypeModel>() {
            @Override
            public void onResponse(Call<CaseTypeModel> call, Response<CaseTypeModel> response) {
                if (response.isSuccessful()) {
                    CaseTypeModel bean = response.body();
                    if (bean.getStatus() == 200) {


                        for (int i = 0; i < bean.getData().size(); i++) {

                            String catName = bean.getData().get(i).getCatName();
                            String catId = bean.getData().get(i).getCatId();


                            spinCaseCat.add(catName);
                            spinCaseCat_id.add(catId);
                        }


                    } else {
                        Toast.makeText(context, "" + bean.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    spCaseCat.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, spinCaseCat));
                    spCategoryyFetch();
                } else {
                    Toast.makeText(context, "Something is wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CaseTypeModel> call, Throwable t) {
                Toast.makeText(context, "Something is wrongs", Toast.LENGTH_SHORT).show();

            }
        });

    }

    /*  method for set cat value in category spinner
     *  Created Date: 15-07-2020
     */

    private void spCategoryyFetch() {
        String myString2 = cat_fetch;//the value you want the position for
        ArrayAdapter myAdap2 = (ArrayAdapter) spCaseCat.getAdapter(); //cast to an ArrayAdapter
        int spinnerPosition = myAdap2.getPosition(myString2);
        //set the default according to value
        spCaseCat.setSelection(spinnerPosition);
    }
    /*  method for open gallery and camera for uploading image- below all 6 methods
     *  Created Date: 13-07-2020
     */

    private void chooseImageFromGallery(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, code);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void takeCameraImage(final int code) {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            fileName = System.currentTimeMillis() + ".jpg";

                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, code);

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case CAMERA_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ivChooseFile.setImageBitmap(photo);
                    getEncoded64ImageStringFromBitmap(photo);
                    userImageBase64 = getEncoded64ImageStringFromBitmap(photo);
                    Log.e("excam", userImageBase64);

                }
                break;
            case GALLERY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Uri imageUri = data.getData();
                    ivChooseFile.setImageURI(imageUri);
                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        userImageBase64 = getEncoded64ImageStringFromBitmap(resizedBitmap);
                        Log.e("exp", userImageBase64);

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                break;

        }
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        Log.e("", imgString);
        return imgString;
    }

    private void openChooser(final int code1, final int code2) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.chooser);
        TextView camera = dialog.findViewById(R.id.camera);
        TextView gallery = dialog.findViewById(R.id.gallery);
        dialog.show();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeCameraImage(code1);
                dialog.dismiss();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImageFromGallery(code2);
                dialog.dismiss();


            }
        });
    }


    /*  method for update case
     *  Created Date: 13-07-2020
     * Updated Date:
     */

    private void update_case() {

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("case_id", getIntent().getStringExtra("caseId"));
            jsonBody.put("user_id", sharedPreference_main.getUserId());
            jsonBody.put("case_category_id", spCaseCatID);
            jsonBody.put("case_title", etTitle.getText().toString());
            jsonBody.put("case_disc", etDetail.getText().toString());
            jsonBody.put("date_from", tvDateFrom.getText().toString());
            jsonBody.put("date_to", tvDateTo.getText().toString());
            jsonBody.put("required_money", etReqAmount.getText().toString());
            jsonBody.put("image", userImageBase64);
            jsonBody.put("city_Id", spCityID);
           /* jsonBody.put("state_id", spStateID);
            jsonBody.put("country_id", spCountryID);*/


        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + UPDATE_CASE, jsonBody,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Integer status = response.getInt("status");
                            if (status == 200) {

                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_LONG).show();
                                finish();


                            } else {

                                Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {

                            e.printStackTrace();
                        }

                        Log.e("response1", response.toString());

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }


}