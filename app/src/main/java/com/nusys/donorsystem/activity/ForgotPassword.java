package com.nusys.donorsystem.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.SharedPreference_main;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.FORGOT_PWD;

/**
 * Created By: Shweta Agarwal
 * Created DAte:22-07-2020
 **/
public class ForgotPassword extends AppCompatActivity {
    Context context;
    SharedPreference_main sharedPreference_main;

    EditText etEmail;
    Button btSubmit;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initialization();
        action();
    }

    public void initialization() {
        context = ForgotPassword.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);

        etEmail = findViewById(R.id.et_email);
        btSubmit = findViewById(R.id.bt_submit);

    }

    public void action() {
        /*redirect to login page when click on btSubmit with validation and  calling forgot password method
         * Created Date: 22-07-2020
         * Updated Date:
         */
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(etEmail.getText().toString())) {

                    etEmail.setError("field can't be empty");

                } else if (!etEmail.getText().toString().matches(emailPattern)) {
                    Toast.makeText(context, "Please Enter The Correct Email", Toast.LENGTH_SHORT).show();
                } else {
                    forgot_pwd();
                }
            }
        });
    }

    /*
     * @return -msg
     * method for retrieve password
     * Created Date: 22-07-2020
     * Updated Date:
     */
    private void forgot_pwd() {


        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {

            jsonBody.put("email", etEmail.getText().toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + FORGOT_PWD, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // Toast.makeText(ChangePassword.this, "true", Toast.LENGTH_SHORT).show();

                            Integer status = response.getInt("status");
                            if (status == 200) {
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, Login.class);
                                context.startActivity(intent);
                            } else {
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
