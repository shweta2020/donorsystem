package com.nusys.donorsystem.activity;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nusys.donorsystem.R;
import com.nusys.donorsystem.adapter.AdapterBlog;
import com.nusys.donorsystem.commonModule.BaseActivity;
import com.nusys.donorsystem.commonModule.DialogProgress;
import com.nusys.donorsystem.commonModule.NetworkUtil;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.BlogModel;
import com.nusys.donorsystem.retrofit.ApiClient;
import com.nusys.donorsystem.retrofit.ServiceInterface;

import retrofit2.Call;
import retrofit2.Callback;

import static com.nusys.donorsystem.commonModule.Extension.showErrorDialog;

/**
 * Created By: Shweta Agarwal
 * Created DAte:02-07-2020
 * Updated Date:07-07-2020(apply functionality, methods, recycler etc.)
 **/
public class Blog extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    Context context;
    SharedPreference_main sharedPreference_main;

    SwipeRefreshLayout refreshLayout;
    LinearLayout errorLayout;
    TextView errorTxtCause;
    Button errorBtnRetry;

    RecyclerView rvActivecaseList;
    AdapterBlog Adapter;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLayoutInflater().inflate(R.layout.activity_blog, frameLayout);
        setTitle("Blog");

        initialization();
        action();
    }

    public void initialization() {
        context = Blog.this;
        sharedPreference_main = SharedPreference_main.getInstance(context);

        pd = new DialogProgress(context, "");
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        refreshLayout = findViewById(R.id.refresh_layout);
        errorLayout = findViewById(R.id.error_layout);
        errorTxtCause = findViewById(R.id.error_txt_cause);
        errorBtnRetry = findViewById(R.id.error_btn_retry);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeColors(Color.RED, Color.YELLOW, Color.BLUE);

        rvActivecaseList = findViewById(R.id.rv_activecaseList);
    }

    public void action() {

        /* call fetch about us method
         * Created Date: 02-07-2020
         */
        fetch_blog();
    }

    private void fetch_blog() {
        if (NetworkUtil.isConnected(context)) {

            pd.show();

            ServiceInterface serviceInterface = ApiClient.getClient().create(ServiceInterface.class);
            Call<BlogModel> call = serviceInterface.fetch_blog();
            call.enqueue(new Callback<BlogModel>() {


                @Override
                public void onResponse(Call<BlogModel> call, retrofit2.Response<BlogModel> response) {

                    if (response.isSuccessful()) {

                        BlogModel bean = response.body();

                        if (bean.getStatus() == 200) {


                            errorLayout.setVisibility(View.GONE);
                            rvActivecaseList.setVisibility(View.VISIBLE);

                            Adapter = new AdapterBlog(context, bean.getData());
                            rvActivecaseList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                            rvActivecaseList.setItemAnimator(new DefaultItemAnimator());
                            rvActivecaseList.setAdapter(Adapter);
                            Log.e("Group_response", bean.toString());
                            pd.dismiss();
                        } else {
                            pd.dismiss();
                            errorLayout.setVisibility(View.VISIBLE);
                            rvActivecaseList.setVisibility(View.GONE);
                            Toast.makeText(context, bean.getMsg(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        pd.dismiss();
                        errorLayout.setVisibility(View.VISIBLE);
                        rvActivecaseList.setVisibility(View.GONE);
                        Toast.makeText(context, "Something is wrong please try again later", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<BlogModel> call, Throwable t) {
                    pd.dismiss();
                    errorLayout.setVisibility(View.VISIBLE);
                    rvActivecaseList.setVisibility(View.GONE);
                    Log.e("error", t.getMessage());
                    Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            showErrorDialog(context, new Dialog(context));
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetch_blog();
                refreshLayout.setRefreshing(false);
            }
        }, 2000);

    }
}
