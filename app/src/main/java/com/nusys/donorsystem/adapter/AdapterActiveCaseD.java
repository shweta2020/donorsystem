package com.nusys.donorsystem.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.donorsystem.R;
import com.nusys.donorsystem.activity.ViewDetailActiveCaseDonor;
import com.nusys.donorsystem.model.ActiveCaseDModel;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created DAte:10-07-2020
 **/

public class AdapterActiveCaseD extends RecyclerView.Adapter<AdapterActiveCaseD.myholder> {

    Context context;
    List<ActiveCaseDModel.Datum> data;

    public AdapterActiveCaseD(Context context, List<ActiveCaseDModel.Datum> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterActiveCaseD.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.active_case_adapter_d, parent, false);
        return new AdapterActiveCaseD.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterActiveCaseD.myholder holder, final int i) {

        holder.tvCity.setText(data.get(i).getCityName());
        holder.tvCaseTitle.setText(data.get(i).getCaseTitle());
        holder.tvCategory.setText(data.get(i).getCatName());
        holder.tvMoney.setText("R "+data.get(i).getRequiredMoney());

        holder.tvView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, ViewDetailActiveCaseDonor.class);
                in.putExtra("caseId", data.get(i).getCaseId());
                context.startActivity(in);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvCategory, tvMoney, tvCaseTitle, tvCity, tvView;


        public myholder(@NonNull View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tv_category);
            tvMoney = itemView.findViewById(R.id.tv_money);
            tvCaseTitle = itemView.findViewById(R.id.tv_case_title);
            tvCity = itemView.findViewById(R.id.tv_city);
            tvView = itemView.findViewById(R.id.tv_view);


        }
    }
}