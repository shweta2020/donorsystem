package com.nusys.donorsystem.adapter;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.ExpandableTextView;
import com.nusys.donorsystem.interfaces.GetCaseIds;
import com.nusys.donorsystem.model.OpenCaseModel;

import java.util.List;

/**
 * Created By: Shweta Agarwal
 * Created DAte:07-07-2020
 * Updated Date:17-07-2020(get case id by interface for chat)
 **/

public class AdapterOpenCase extends RecyclerView.Adapter<AdapterOpenCase.myholder> {

    Context context;
    List<OpenCaseModel.Datum> data;
    GetCaseIds getCaseIds;

    public AdapterOpenCase(Context context, List<OpenCaseModel.Datum> data, GetCaseIds getCaseIds) {
        this.context = context;
        this.data = data;
        this.getCaseIds = getCaseIds;
    }

    @NonNull
    @Override
    public AdapterOpenCase.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.open_case_adapter, parent, false);
        return new AdapterOpenCase.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterOpenCase.myholder holder, final int i) {

        holder.tvHeading.setText(data.get(i).getCaseTitle());
        holder.tvAmount.setText("R "+data.get(i).getRequiredMoney());

        Glide.with(context)
                .load(data.get(i).getImg())
                .placeholder(R.drawable.user_icon)
                .into(holder.ivImg);
        holder.etvDisc.setText(Html.fromHtml(data.get(i).getCaseDisc()).toString());
        holder.etvDisc.setAnimationDuration(750L);
        holder.etvDisc.setInterpolator(new OvershootInterpolator());
        holder.etvDisc.setExpandInterpolator(new OvershootInterpolator());
        holder.etvDisc.setCollapseInterpolator(new OvershootInterpolator());
        holder.tvViewmore.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onClick(final View v) {
                holder.tvViewmore.setText(holder.etvDisc.isExpanded() ? R.string.expand : R.string.collapse);
                holder.etvDisc.toggle();
            }
        });


        holder.tvViewmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (holder.etvDisc.isExpanded()) {
                    holder.etvDisc.collapse();
                    holder.tvViewmore.setText(R.string.expand);
                } else {
                    holder.etvDisc.expand();
                    holder.tvViewmore.setText(R.string.collapse);
                }
            }
        });


        holder.etvDisc.addOnExpandListener(new ExpandableTextView.OnExpandListener() {
            @Override
            public void onExpand(@NonNull final ExpandableTextView view) {
                Log.d("texzt", "ExpandableTextView expanded");
            }

            @Override
            public void onCollapse(@NonNull final ExpandableTextView view) {
                Log.d("texzt", "ExpandableTextView collapsed");
            }
        });

        holder.tvChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCaseIds.getCaseIds(data.get(i).getCaseId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvHeading, tvViewmore, tvAmount, tvPay, tvChat;
        ImageView ivImg;
        ExpandableTextView etvDisc;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvHeading = itemView.findViewById(R.id.tv_heading);
            etvDisc = itemView.findViewById(R.id.etv_disc);
            tvViewmore = itemView.findViewById(R.id.tv_viewmore);
            ivImg = itemView.findViewById(R.id.iv_img);
            tvAmount = itemView.findViewById(R.id.tv_amount);
            tvPay = itemView.findViewById(R.id.tv_pay);
            tvChat = itemView.findViewById(R.id.tv_chat);

        }
    }
}