package com.nusys.donorsystem.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.ActiveCaseRModel;
import com.nusys.donorsystem.model.GetChatModel;
import com.squareup.picasso.Picasso;


import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created By: Shweta Agarwal
 * Created DAte:17-07-2020
 * Updated Date:20-07-2020(add api and fetch values)
 **/


public class AdapterChatComment extends RecyclerView.Adapter<AdapterChatComment.myholder> {
    Context context;
    List<GetChatModel.Datum> data;

    public AdapterChatComment(Context context, List<GetChatModel.Datum> data) {
        this.context = context;
        this.data = data;

    }

    @Override
    public AdapterChatComment.myholder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.chat_adapter, parent, false);
        return new AdapterChatComment.myholder(view);

    }


    @Override
    public void onBindViewHolder(AdapterChatComment.myholder holder, final int i) {

        if (data.get(i).getUserId().equals("1")) {

            if (data.get(i).getMsgType().equals("image")) {

                Glide.with(context).load(data.get(i).getMsgData()).into(holder.ivCommentInImage);
                holder.ivCommentInImage.setVisibility(View.VISIBLE);
                holder.tvComments.setVisibility(View.GONE);

            } else if (data.get(i).getMsgType().equals("text")) {

                holder.tvComments.setText(data.get(i).getMsgData());
                holder.tvComments.setVisibility(View.VISIBLE);
                holder.ivCommentInImage.setVisibility(View.GONE);

            } else {
                Toast.makeText(context, "abcd", Toast.LENGTH_SHORT).show();
            }


            holder.tvCommenterName.setText(data.get(i).getUsername());
            holder.tvTime.setText(data.get(i).getTime());

            Glide.with(context)
                    .load(data.get(i).getProfilePic())
                    .placeholder(R.drawable.user_icon)
                    .into(holder.civ_commenter_image);
            holder.rlLeft.setVisibility(View.VISIBLE);
            holder.rlReverse.setVisibility(View.GONE);

        } else {

            if (data.get(i).getMsgType().equals("image")) {

                Glide.with(context).load(data.get(i).getMsgData()).into(holder.ivCommentInImgReverse);
                holder.ivCommentInImgReverse.setVisibility(View.VISIBLE);
                holder.tvCommentReverse.setVisibility(View.GONE);
            } else if (data.get(i).getMsgType().equals("text")) {

                holder.tvCommentReverse.setText(data.get(i).getMsgData());
                holder.tvCommentReverse.setVisibility(View.VISIBLE);
                holder.ivCommentInImgReverse.setVisibility(View.GONE);
            } else {
                Toast.makeText(context, "abc" + data.get(i).getUserId() + data.get(i).getMsgType() + i + data.get(i).getMsgType(), Toast.LENGTH_LONG).show();

            }
            holder.tvCommenterNameReverse.setText(data.get(i).getUsername());
            holder.tvCommentTimeReverse.setText(data.get(i).getTime());
            if (data.get(i).getProfilePic().equals("null")) {
                holder.civCommenterImageReverse.setImageResource(R.mipmap.ic_launcher);
            } else {
                Glide.with(context)
                        .load(data.get(i).getProfilePic())
                        .placeholder(R.drawable.user_icon)
                        .into(holder.civCommenterImageReverse);
            }

            holder.rlLeft.setVisibility(View.GONE);
            holder.rlReverse.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvCommenterName, tvComments, tvTime;
        CircleImageView civ_commenter_image, civCommenterImageReverse;
        RelativeLayout rlReverse, rlLeft;

        ImageView ivCommentInImage, ivCommentInImgReverse;
        TextView tvCommenterNameReverse, tvCommentTimeReverse, tvCommentReverse;

        public myholder(@NonNull View itemView) {
            super(itemView);
            civ_commenter_image = itemView.findViewById(R.id.civ_commenter_image);
            tvCommenterName = itemView.findViewById(R.id.tv_user_name);
            tvComments = itemView.findViewById(R.id.tv_comments);
            tvTime = itemView.findViewById(R.id.tv_time);
            ivCommentInImage = itemView.findViewById(R.id.iv_comment_in_image);
            rlLeft = itemView.findViewById(R.id.rl_left);

            civCommenterImageReverse = itemView.findViewById(R.id.civ_commenter_image_reverse);
            tvCommenterNameReverse = itemView.findViewById(R.id.tv_user_name_reverse);
            tvCommentTimeReverse = itemView.findViewById(R.id.tv_commentTime_reverse);
            tvCommentReverse = itemView.findViewById(R.id.tv_comment_reverse);
            ivCommentInImgReverse = itemView.findViewById(R.id.iv_comment_in_img_reverse);
            rlReverse = itemView.findViewById(R.id.rl_reverse);


        }
    }

}