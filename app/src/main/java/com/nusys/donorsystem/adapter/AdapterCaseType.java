package com.nusys.donorsystem.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.activity.OpenCase;
import com.nusys.donorsystem.model.CaseTypeModel;

import java.util.List;


/**
 * Created By: Shweta Agarwal
 * Created DAte:01-07-2020
 **/
public class AdapterCaseType extends RecyclerView.Adapter<AdapterCaseType.myholder> {

    Context context;
    List<CaseTypeModel.Datum> data;

    public AdapterCaseType(Context context, List<CaseTypeModel.Datum> data) {
        this.context = context;
        this.data = data;

    }

    @NonNull
    @Override
    public AdapterCaseType.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.open_case, parent, false);
        return new AdapterCaseType.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCaseType.myholder holder, final int position) {


        holder.tvHeading.setText(data.get(position).getCatName());
        Glide.with(context)
                .load(data.get(position).getImg())
                .placeholder(R.drawable.user_icon)
                .into(holder.imgType);
        holder.rlCase.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, OpenCase.class);
                i.putExtra("cate_id", data.get(position).getCatId());
                context.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvHeading;
        ImageView imgType;
        RelativeLayout rlCase;

        public myholder(@NonNull View itemView) {
            super(itemView);
            tvHeading = itemView.findViewById(R.id.tv_heading);
            imgType = itemView.findViewById(R.id.img_type);
            rlCase = itemView.findViewById(R.id.rl_case);

        }
    }
}