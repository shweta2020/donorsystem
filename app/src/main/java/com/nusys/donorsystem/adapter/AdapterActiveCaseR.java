package com.nusys.donorsystem.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nusys.donorsystem.R;
import com.nusys.donorsystem.activity.EditAddCase;
import com.nusys.donorsystem.activity.MainActivity;
import com.nusys.donorsystem.activity.UpdateKyc;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.ActiveCaseRModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nusys.donorsystem.commonModule.Constants.BASE_URL;
import static com.nusys.donorsystem.commonModule.Constants.CASE_DELETE;

/**
 * Created By: Shweta Agarwal
 * Created DAte:10-07-2020
 **/

public class AdapterActiveCaseR extends RecyclerView.Adapter<AdapterActiveCaseR.myholder> {
    Context context;
    List<ActiveCaseRModel.Datum> data;
    SharedPreference_main sharedPreference_main;

    public AdapterActiveCaseR(Context context, List<ActiveCaseRModel.Datum> data) {
        this.context = context;
        this.data = data;
        sharedPreference_main = SharedPreference_main.getInstance(context);
    }

    @NonNull
    @Override
    public AdapterActiveCaseR.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.active_case_adapter, parent, false);
        return new AdapterActiveCaseR.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterActiveCaseR.myholder holder, final int i) {

        holder.tvCity.setText(data.get(i).getCityName());
        holder.tvCaseTitle.setText(data.get(i).getCaseTitle());
        holder.tvCategory.setText(data.get(i).getCatName());
        holder.tvMoney.setText("R "+data.get(i).getRequiredMoney());
        if (data.get(i).getStatus().equals("1")) {
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatusPending.setVisibility(View.GONE);
        } else {
            holder.tvStatus.setVisibility(View.GONE);
            holder.tvStatusPending.setVisibility(View.VISIBLE);
        }
        if (data.get(i).getKYCStatus().equals("1")) {
            holder.tvKycStatus.setVisibility(View.VISIBLE);
            holder.tvKycStatusPending.setVisibility(View.GONE);
        } else {
            holder.tvKycStatus.setVisibility(View.GONE);
            holder.tvKycStatusPending.setVisibility(View.VISIBLE);
        }

        holder.tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, EditAddCase.class);

                in.putExtra("caseId", data.get(i).getId());
                context.startActivity(in);
            }
        });
        holder.tvKycEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(context, UpdateKyc.class);

                in.putExtra("caseId", data.get(i).getId());
                context.startActivity(in);
            }
        });

        /* dialog box for delete case
         * Created Date: 15-07-2020
         * Updated Date:
         */
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.delete);
                dialog.getWindow().getDecorView().setTop(100);
                dialog.getWindow().getDecorView().setLeft(100);
                dialog.show();

                TextView tvText = dialog.findViewById(R.id.tv_text);
                //  tvText.setText("Are you sure you want to Submit?");
                Button bt_delete = dialog.findViewById(R.id.delete);
                //  bt_delete.setText("Submit");
                Button bt_cancel = dialog.findViewById(R.id.cancel);
                bt_delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*calling method for delete case
                         * Created Date: 15-07-2020
                         * Updated Date:
                         */
                        delete_case(data.get(i).getId());
                        dialog.dismiss();

                    }
                });
                bt_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

    }

    /*
     * @param - case_id
     * @return -msg
     * method for delete case
     * Created Date: 15-07-2020
     * Updated Date:
     */
    private void delete_case(String case_id) {

        RequestQueue requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("case_id", case_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + CASE_DELETE, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Integer status = response.getInt("status");
                            if (status == 200) {
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, MainActivity.class);
                                context.startActivity(intent);
                            } else {
                                Toast.makeText(context, "" + response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", sharedPreference_main.getToken());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvCategory, tvMoney, tvCaseTitle, tvCity, tvKycStatus, tvStatus, tvStatusPending, tvKycStatusPending;
        TextView tvDelete, tvEdit, tvKycEdit;


        public myholder(@NonNull View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tv_category);
            tvMoney = itemView.findViewById(R.id.tv_money);
            tvCaseTitle = itemView.findViewById(R.id.tv_case_title);
            tvCity = itemView.findViewById(R.id.tv_city);
            tvKycStatus = itemView.findViewById(R.id.tv_kyc_status);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvStatusPending = itemView.findViewById(R.id.tv_status_pending);
            tvKycStatusPending = itemView.findViewById(R.id.tv_kyc_status_pending);
            tvDelete = itemView.findViewById(R.id.tv_delete);
            tvEdit = itemView.findViewById(R.id.tv_edit);
            tvKycEdit = itemView.findViewById(R.id.tv_kyc_edit);

        }
    }
}