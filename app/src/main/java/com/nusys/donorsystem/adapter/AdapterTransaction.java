package com.nusys.donorsystem.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nusys.donorsystem.R;
import com.nusys.donorsystem.commonModule.SharedPreference_main;
import com.nusys.donorsystem.model.TransactionModel;

import java.util.List;


/**
 * Created By: Shweta Agarwal
 * Created DAte:16-07-2020
 * Updated Date:20-07-2020(add apis and functionality)
 **/

public class AdapterTransaction extends RecyclerView.Adapter<AdapterTransaction.myholder> {

    Context context;
    List<TransactionModel.Datum> data;
    SharedPreference_main sharedPreference_main;

    public AdapterTransaction(Context context, List<TransactionModel.Datum> data) {
        this.context = context;
        this.data = data;
        sharedPreference_main = SharedPreference_main.getInstance(context);
    }

    @NonNull
    @Override
    public AdapterTransaction.myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.transaction_adapter, parent, false);
        return new AdapterTransaction.myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterTransaction.myholder holder, final int i) {

        holder.tvCategory.setText(data.get(i).getCatName());
        holder.tvMoney.setText("R "+data.get(i).getRequiredMoney());
        holder.tvCaseTitle.setText(data.get(i).getCaseTitle());
        holder.tvCity.setText(data.get(i).getCityName());
        holder.tvDonatedAmount.setText("R "+data.get(i).getAmount());

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class myholder extends RecyclerView.ViewHolder {
        TextView tvCategory, tvMoney, tvCaseTitle, tvCity, tvDonatedAmount;


        public myholder(@NonNull View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tv_category);
            tvMoney = itemView.findViewById(R.id.tv_money);
            tvCaseTitle = itemView.findViewById(R.id.tv_case_title);
            tvCity = itemView.findViewById(R.id.tv_city);
            tvDonatedAmount = itemView.findViewById(R.id.tv_donated_amount);


        }
    }
}