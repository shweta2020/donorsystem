package com.nusys.donorsystem.retrofit;

import com.nusys.donorsystem.model.AboutModel;
import com.nusys.donorsystem.model.ActiveCaseDModel;
import com.nusys.donorsystem.model.ActiveCaseRModel;
import com.nusys.donorsystem.model.BlogModel;
import com.nusys.donorsystem.model.CaseByIDModel;
import com.nusys.donorsystem.model.GetChatModel;
import com.nusys.donorsystem.model.OpenCaseModel;
import com.nusys.donorsystem.model.CaseTypeModel;
import com.nusys.donorsystem.model.CityModel;
import com.nusys.donorsystem.model.CountryModel;
import com.nusys.donorsystem.model.LoginModel;
import com.nusys.donorsystem.model.ProfileModel;
import com.nusys.donorsystem.model.ServiceModel;
import com.nusys.donorsystem.model.StateModel;
import com.nusys.donorsystem.model.TransactionModel;
import com.nusys.donorsystem.model.UserTypeModel;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

import static com.nusys.donorsystem.commonModule.Constants.ABOUT;
import static com.nusys.donorsystem.commonModule.Constants.ACTIVE_CASE_DONOR;
import static com.nusys.donorsystem.commonModule.Constants.ACTIVE_CASE_RECEIVER;
import static com.nusys.donorsystem.commonModule.Constants.BLOG;
import static com.nusys.donorsystem.commonModule.Constants.CASE_BY_ID;
import static com.nusys.donorsystem.commonModule.Constants.CASE_CATEGORY;
import static com.nusys.donorsystem.commonModule.Constants.CITY_LIST;
import static com.nusys.donorsystem.commonModule.Constants.COUNTRY_LIST;
import static com.nusys.donorsystem.commonModule.Constants.GET_CHAT;
import static com.nusys.donorsystem.commonModule.Constants.GET_TRANSACTION;
import static com.nusys.donorsystem.commonModule.Constants.LOGIN;
import static com.nusys.donorsystem.commonModule.Constants.OPEN_CASE;
import static com.nusys.donorsystem.commonModule.Constants.PROFILE;
import static com.nusys.donorsystem.commonModule.Constants.SERVICES;
import static com.nusys.donorsystem.commonModule.Constants.STATE_LIST;
import static com.nusys.donorsystem.commonModule.Constants.USER_TYPE;

/**
 * Created By: Shweta Agarwal
 * Created Date:01-07-2020
 * Updated Date:
 */
public interface ServiceInterface {

    @POST(LOGIN)
    Call<LoginModel> doLogin(@Body HashMap<String, String> map);

    //**view type of cases**//
    @GET(CASE_CATEGORY)
    Call<CaseTypeModel> case_category(@Header("token") String token, @Header("Content_Type") String header);

    //**view country list**//
    @GET(COUNTRY_LIST)
    Call<CountryModel> country_list(@Header("Content_Type") String header);

    //**view state list**//
    @POST(STATE_LIST)
    Call<StateModel> state_list(@Body HashMap<String, String> map);

    //**view city list**//
    @POST(CITY_LIST)
    Call<CityModel> city_list(@Body HashMap<String, String> map);

    //**view user type list**//
    @GET(USER_TYPE)
    Call<UserTypeModel> user_type(@Header("Content_Type") String header);

    //**view user type list**//
    @POST(PROFILE)
    Call<ProfileModel> profile(@Header("token") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //**view open case list**//
    @POST(OPEN_CASE)
    Call<OpenCaseModel> open_case(@Header("token") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //**view active case list**//
    @POST(ACTIVE_CASE_DONOR)
    Call<ActiveCaseDModel> active_case_donor(@Header("token") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //**view active case receiver list**//
    @POST(ACTIVE_CASE_RECEIVER)
    Call<ActiveCaseRModel> active_case_receiver(@Header("token") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //**view active case receiver list**//
    @POST(GET_CHAT)
    Call<GetChatModel> get_chat(@Header("token") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //**view case by id**//
    @POST(CASE_BY_ID)
    Call<CaseByIDModel> fetch_case_byId(@Header("token") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //**transaction list**//
    @POST(GET_TRANSACTION)
    Call<TransactionModel> get_transaction(@Header("token") String token, @Header("Content_Type") String header, @Body HashMap<String, String> map);

    //**view about**//
    @GET(ABOUT)
    Call<AboutModel> fetch_about(@Header("Content_Type") String header);

    //**view services**//
    @GET(SERVICES)
    Call<ServiceModel> fetch_services();

    //**view blogs**//
    @GET(BLOG)
    Call<BlogModel> fetch_blog();

}
