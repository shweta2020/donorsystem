package com.nusys.donorsystem.interfaces;

public interface GetCaseIds {

    public void getCaseIds(String caseId);
}
